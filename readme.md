<p align="center">Warranty Application</p>

<p align="center">
<p>Version: 0.25b</p>
<p>LTS: none</p>
</p>

## About Warranty Application
Features and Capabilities(for Version 1.0)
<ul>
    <li>Secure Sign In</li>
    <li>Admin/User Dashboard</li>
    <li>Admin Warranty Lookup</li>
    <li>Admin Warranty Edit</li>
    <li>Admin Warranty Transfer</li>
    <li>Admin User Administration</li>
    <li>Admin Franchise Administration</li>
    <li>Admin Survey Creation</li>
    <li>Admin Reports</li>
    <li>Franchise User Warranty Submission</li>
    <li>Franchise User Warranty Lookup</li>
    <li>Franchise User Reports</li>
    <li>Franchise User Request</li>
    <li>Print & Export Capabilities</li>
    <li>Responsive Capability</li>
</ul>

## Progression
<h3>Version 0.27b</h3>
<ul>
    <li>Creation of Base Warranty Table</li>
    <li>Submission of Warranty From to database</li>
    <li>Listing of Warranties</li>
    <li>Showing of basic warranty information</li>
</ul>

<h3>Version 0.26b</h3>
<ul>
    <li>Creation of warranty type picker</li>
    <li>Creation of warranty type forms</li>
    <li>Population of forms based on type picker</li>
    <li>Editing Additional Contacts From Customer Management</li>
</ul>

<h3>Version 0.25b</h3>
<ul>
    <li>Creation of Additional Contacts for Customers From Customer Management</li>
    <li>Deletion of Contacts From Customer Management</li>
    <li>Setting of Master Contact From Customer Management</li>
    <li>Editing Additional Contacts From Customer Management</li>
</ul>

<h3>Version 0.24b</h3>
<ul>
    <li>Selection of Customer in Warranty Creation Pane</li>
    <li>Selection of Customer Address in Warranty Creation Pane</li>
    <li>Creation of additional customer and address in Warranty Creation Pane</li>
    <li>Redirect after customer creation allowing for saved state and progression to warranty type picker</li>
</ul>

<h3>Version 0.23b</h3>
<ul>
    <li>Creation of Additional Addresses for Customers From Customer Management</li>
    <li>Deletion of Addresses From Customer Management</li>
    <li>Setting of Master Address From Customer Management</li>
    <li>Editing Additional Addresses From Customer Management</li>
</ul>


<h3>Version 0.22b</h3>
<ul>
    <li>Editing of Main Address and Details for Customer</li>
    <li>Deletion of Franchises</li>
    <li>Deletion of Users</li>
    <li>Deletion of Customers</li>
    <li>Customer Live Search</li>
</ul>

<h3>Version 0.21b</h3>
<ul>
    <li>Allow for multiple addresses for customers</li>
    <li>Creation and editing of customers.</li>
</ul>

<h3>Version 0.2b</h3>
<ul>
    <li>Creation and Editing of Franchise with Basic Information</li>
    <li>Creation, Editing, and linking of Customer with Basic Information with a single Address</li>
    <li>Login scripts restricting access to buttons based on account typeCreate</li>
    <li>Only show customers of a specific franchise when that franchise is logged in.</li>
</ul>
