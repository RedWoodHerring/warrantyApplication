<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $fillable = [
        'franchise_id','contacts','name'
    ];

    protected $searchable = [
        'fName','lName'
    ];

    public function franchise()
    {
        return $this->belongsTo('App\Franchise','franchise_id');
    }

    public function addresses()
    {
        return $this->hasMany('App\Address', 'customer_id');
    }
}
