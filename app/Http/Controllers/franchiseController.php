<?php

namespace App\Http\Controllers;

use App\Franchise;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;

class franchiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Table Data
        $data = Franchise::all();


        //Data Manipulation
        foreach($data as $row){
            $userInfo = $row->user()->get();
            $user;
            foreach ($userInfo as $temp){
                $user = $temp;
            }
            $row['DT_RowId'] = $row['id'];
            $row['edit'] = "<a class='btn yellow edit'>Edit</a>";
            $json = json_decode($row['data'],true);
            $row['name'] = $user['name'];
            $row['address'] = $json['address'];
            $row['phone'] = $json['phone'];
            unset($row['data']);
        }

        //Table Column Grab
        $columns[] = (object) array('data' => 'name');
        $columns[] = (object) array('data' => 'address');
        $columns[] = (object) array('data' => 'phone');
        $columns[] = (object) array('data' => 'edit');

        //Table Headers
        $headers = array('Name','Address','Phone','Edit');

        $pageName = "Franchise Listing";

        //newForm
        $form = $this->create();
        //View
        return view('listing', compact('data', 'headers', 'columns', 'pageName','form'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.franchiseCreate')->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $this->validate($request, [
            'username'=>'unique:users'
        ]);

        $user['name'] = $request->name;
        $user['username'] = $request->username;
        $user['password'] = bcrypt($request->password);
        $user['type'] = 'F';

        $id = User::create($user)->id;


        $data['name'] = $request->name;
        $data['address'] = $request->address;
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['zip'] = $request->zip;
        $data['phone'] = $request->phone;
        $data['tollFree'] = $request->tollFree;
        $data['fax'] = $request->fax;

        $franchise['data'] = json_encode($data);
        $franchise['user_id'] = $id;

        Franchise::create($franchise);

        return redirect('/franchises');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $franchise = Franchise::find($id);
        $userInfo = $franchise->user()->get();
        $user;
        foreach ($userInfo as $row){
            $user = $row;
        }

        $json = json_decode($franchise['data'],true);
        // Data Manipulation
        $data['name'] = $user['name'];
        $data['username'] = $user['username'];
        $data['columns'] = array('Address','City','State','ZipCode','Phone Number','Toll Free Number','Fax Number');
        $data['address'] = "";
        $data['city'] = "";
        $data['state'] = "";
        $data['zip'] = "";
        $data['standard'] = "";
        $data['tollFree'] = "";
        $data['fax'] = "";

        foreach($json as $key => $value){
            $data[$key] = $value;
        }

        $route = "/franchises/".$id;

        $pageName = "Franchise Details";

        $type = 'franchise';
        // View
        return view('franchises.details', compact('data', 'pageName','route','type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $franchise = Franchise::find($id);
        $form = $request->all();
        $data = json_decode($franchise['data'],true);
        if(isset($form['address'])){
            $data['address'] = $form['address'];
        }
        if(isset($form['city'])){
            $data['city'] = $form['city'];
        }
        if(isset($form['state'])){
            $data['state'] = $form['state'];
        }
        if(isset($form['zip'])){
            $data['zip'] = $form['zip'];
        }
        if(isset($form['phone'])){
            $data['phone'] = $form['phone'];
        }
        if(isset($form['tollFree'])){
            $data['tollFree'] = $form['tollFree'];
        }
        if(isset($form['fax'])){
            $data['fax'] = $form['fax'];
        }
        if(!isset($form['name']) && !isset($form['username'])){
            $update['data'] = json_encode($data,true);
            $franchise->fill($update)->save();
        } else {
            $update = $form;
            $user = User::find($franchise['user_id']);
            $user->fill($update)->save();
        }
        return redirect()->route('franchises.show', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $franchise = Franchise::find($id);
        $user = User::find($franchise->user_id);
        $user->delete();
        $franchise->delete();
        return redirect()->route('franchises.index');
    }
}
