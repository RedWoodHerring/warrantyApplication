<?php

namespace App\Http\Controllers;

use App\User;
use App\Franchise;
use App\Http\Requests;
use Illuminate\Http\Request;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Table Data
        $data = User::all();

        //Data Manipulation
        foreach($data as $row){
            $row['DT_RowId'] = $row['id'];
            $row['edit'] = "<button class='btn yellow edit'>Edit</button><button class='btn blue info'>Info</button>";
            if($row['type'] === 'A'){
                $row['type'] = 'Admin';
            }
            if($row['type'] === 'F'){
                $row['type'] = 'Franchise';
            }
            if($row['type'] === 'C'){
                $row['type'] = 'Customer';
            }
        }

        //Table Column Grab
        $columns[] = (object) array('data' => 'name');
        $columns[] = (object) array('data' => 'username');
        $columns[] = (object) array('data' => 'type');
        $columns[] = (object) array('data' => 'edit');

        //Table Headers
        $headers = array('Name','User Name','Type','Functions');

        $pageName = "User Listing";

        //newForm
        $form = $this->create();

        //View
        return view('listing', compact('data', 'headers', 'columns', 'pageName','form'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.userType')->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->all();
        $data = $this->validate($request, [
            'username'=>'required|unique:users',
            'password'=>'required'
        ]);
        $user['password'] = bcrypt($request->password);
        User::create($user);
        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Table Data
        $user = User::find($id);

        //Data Manipulation
        $data['name'] = $user['name'];
        $data['username'] = $user['username'];

        if($user['type'] === 'A'){
            $data['type'] = 'Admin';
        }
        if($user['type'] === 'F'){
            $data['type'] = 'Franchise';
        }
        if($user['type'] === 'C'){
            $data['type'] = 'Customer';
        }

        $route = "/users/".$id;

        $pageName = "User Details";

        $type = 'user';

        //View
        return view('users.details', compact('data', 'pageName','route','type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $data = $this->validate($request, [
            'username'=>'unique:users'
        ]);
        $data = $request->all();
        $user->fill($data)->save();
        return redirect()->route('users.show', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $type = $user->type;
        if($type === 'F'){
            Franchise::where('user_id', '=', $user->id)->first()->delete();
        }
        $user->delete();
        return redirect()->route('users.index');
    }

    public function typeCreate($type)
    {
        if($type === "A"){
            return view('forms.userCreate',['type' => $type])->render();
        }
        if($type === "F"){
            return view('forms.franchiseCreate',['type' => $type])->render();
        }
    }

}
