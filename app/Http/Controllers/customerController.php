<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Franchise;
use App\Address;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class customerController extends Controller
{
    //
    // Customer listing
    //

    public function index()
    {

        $currentUser = Auth::user();
        //Table Data

        if($currentUser['type'] === 'A'){
            $data = Customer::all();
        }else if($currentUser['type'] === 'F'){
            $id = $currentUser['id'];
            $franchise = Franchise::where('user_id', $id)->first();
            $temp = $franchise->toArray();
            $data = Customer::where('franchise_id', $temp['id'])->get();
        }

        //Data Manipulation
        foreach($data as $row){
            $row['DT_RowId'] = $row['id'];
        }

        //Return Data
        return $data;

    }

    public function listing()
    {
        $currentUser = Auth::user();
        //Table Data

        if($currentUser['type'] === 'A'){
            $data = Customer::all();
        }else if($currentUser['type'] === 'F'){
            $id = $currentUser['id'];
            $franchise = Franchise::where('user_id', $id)->first();
            $temp = $franchise->toArray();
            $data = Customer::where('franchise_id', $temp['id'])->get();
        }

        //Data Manipulation
        foreach($data as $row){
            $row['DT_RowId'] = $row['id'];
            $row['edit'] = "<a class='btn yellow edit'>Details</a><a class='btn blue address'>Addresses</a><a class='btn blue warranty'>Warranties</a>";
            $json = json_decode($row['contacts'],true);
            foreach($json as $contact){
                if($contact['type'] === 'Primary'){
                    $row['email'] = $contact['email'];
                }

    }
}

//Table Column Grab
$columns[] = (object) array('data' => 'name');
$columns[] = (object) array('data' => 'email');
$columns[] = (object) array('data' => 'edit');

//Table Headers
$headers = array('Customer Name','Primary Email','Edit');

$pageName = "Customer Listing";

//newForm
$form = $this->create();

//View
return view('customers.listing', compact('data', 'headers', 'columns', 'pageName','form'));
    }

    //
    // Customer Creation Form
    //

    public function create()
    {
        $franchises = Franchise::all();
        foreach($franchises as $franchise){
            $userInfo = $franchise->user()->get();
            $user;
            foreach ($userInfo as $row){
                $user = $row;
            }
            $franchise['name'] = $user['name'];
        }
        return view('customers.create',compact('franchises'))->render();
    }

    public function createSingle()
    {
        $franchises = Franchise::all();
        foreach($franchises as $franchise){
            $userInfo = $franchise->user()->get();
            $user;
            foreach ($userInfo as $row){
                $user = $row;
            }
            $franchise['name'] = $user['name'];
        }
        return view('forms.customerCreate',compact('franchises'))->render();
    }

    //
    // Customer Creation
    //

    public function store(Request $request)
    {
        $contact[$request->fName.'-'.$request->lName]['fName'] = $request->fName;
        $contact[$request->fName.'-'.$request->lName]['lName'] = $request->lName;
        $contact[$request->fName.'-'.$request->lName]['email'] = $request->email;
        $contact[$request->fName.'-'.$request->lName]['phone'] = $request->phone;
        $contact[$request->fName.'-'.$request->lName]['type'] = "Primary";

        $customer['contacts'] = json_encode($contact);

        $customer['name'] = $request->name;

        $customer['franchise_id'] = $request->franchise;

        $id = Customer::create($customer)->id;
        $address['address'] = $request->address;
        $address['city'] = $request->city;
        $address['state'] = $request->state;
        $address['zip'] = $request->zip;
        $address['county'] = $request->county;
        $address['type'] = 'M';
        $main['data'] = json_encode($address,true);
        $main['customer_id'] = $id;

        $address = Address::create($main)->id;

        if(isset($request->redirect)){
            return redirect('/warranties/create/newCustomer/'.$id.'/'.$address);
        } else {
            return redirect('/customers/listing');
        }
    }

    //
    // Customer Details
    //

    public function show($id)
    {
        $customer = Customer::find($id);
        $franchiseInfo = $customer->franchise()->get();
        $franchise;
        $user;
        foreach ($franchiseInfo as $row){
            $franchise = $row;
            $userInfo = $row->user()->get();
            foreach ($userInfo as $temp){
                $user = $temp;
            }
        }

        $json = json_decode($customer['contacts'],true);
        $data['name'] = $customer['name'];

        // Data Manipulation
        foreach($json as $contact){
            if($contact['type'] === 'Primary'){
                $data['fName'] = $contact['fName'];
                $data['lName'] = $contact['lName'];
                $data['phone'] = $contact['phone'];
                $data['email'] = $contact['email'];
            }
        }

        $addresses = $customer->addresses()->get();

        foreach($addresses as $address){
            $json = json_decode($address['data'],true);
            if($json['type'] === 'M'){
                $data['address'] = $json['address'];
                $data['city'] = $json['city'];
                $data['state'] = $json['state'];
                $data['zip'] = $json['zip'];
                $data['county'] = $json['county'];
            }


        }
        $data['franchise'] = $user['name'];
        $route = "/customers/".$id;

        $pageName = "Customer Details";

        $type = 'customer';

        $baseHref = "/customers/".$id;

        // View
        return view('customers.details', compact('data', 'pageName','route','type','baseHref'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    //
    // Customer Update Info
    //

    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $form = $request->all();
        $customer->fill($form)->save();
        return redirect()->route('customers.show', ['id' => $id]);
    }

    //
    // Customer Deletion
    //
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        return redirect()->route('customers.index');
    }


    //
    // Customer Search
    //

    public function search($search){
        // return $search;

        $currentUser = Auth::user();
        //Table Data

        if($currentUser['type'] === 'A'){
            $customers = Customer::where('name', 'LIKE', '%'. $search .'%')->get();
        }else if($currentUser['type'] === 'F'){
            $id = $currentUser['id'];
            $franchise = Franchise::where('user_id', $id)->first();
            $temp = $franchise->toArray();
            $customers = Customer::where('franchise_id', $temp['id'])->where('name', 'LIKE', '%'. $search .'%')->get();
        }

        return $customers;
    }

    public function warranties($id)
    {

        $route = "/customers/".$id;

        $pageName = "Customer Details";

        $type = 'customer';

        $baseHref = "/customers/".$id;


        // View
        return view('customers.warranties', compact('pageName','route','type','baseHref'));
    }

    //
    // Contacts CRUD
    //

    public function contacts($id)
    {
        $customer = Customer::find($id);

        $contacts = json_decode($customer['contacts'],true);

        $route = "/customers/".$id;

        $pageName = "Customer Details";

        $type = 'customer';

        $baseHref = "/customers/".$id;

        // View
        return view('customers.contacts', compact('pageName','route','type','baseHref','contacts','id'));
    }

    public function updateContact(Request $request, $id)
    {
        $key = $request->key;
        $fname = $request->fName;
        $lname = $request->lName;
        $email = $request->email;
        $phone = $request->phone;
        $customer = Customer::find($id);
        $contacts = json_decode($customer['contacts'],true);

        $contacts[$key]['fName'] = $fname;
        $contacts[$key]['lName'] = $lname;
        $contacts[$key]['email'] = $email;
        $contacts[$key]['phone'] = $phone;

        $customer['contacts'] = json_encode($contacts,true);
        $customer->save();

        return redirect()->route('customers.contacts', ['id' => $id]);
    }

    public function createContact($id)
    {
        return view('forms.contactCreate', compact('id'));
    }

    public function storeContact(Request $request, $id)
    {
        $customer = Customer::find($id);

        $contacts = json_decode($customer['contacts'],true);

        $contacts[$request->fName.'-'.$request->lName]['fName'] = $request->fName;
        $contacts[$request->fName.'-'.$request->lName]['lName'] = $request->lName;
        $contacts[$request->fName.'-'.$request->lName]['email'] = $request->email;
        $contacts[$request->fName.'-'.$request->lName]['phone'] = $request->phone;
        $contacts[$request->fName.'-'.$request->lName]['type'] = "Other";

        $customer['contacts'] = json_encode($contacts,true);
        $customer->save();
        return redirect()->route('customers.contacts', ['id' => $id]);
    }

    public function primaryContact($id, $contactKey)
    {
        $name = $contactKey;
        $customer = Customer::find($id);
        $contacts = json_decode($customer['contacts'],true);
        foreach($contacts as $key => $contact){
            if($key === $name){
                $contacts[$key]['type'] = 'Primary';
            } else {
                $contacts[$key]['type'] = 'Other';
            }
        }
        $customer['contacts'] = json_encode($contacts,true);
        $customer->save();
        return redirect()->route('customers.contacts', ['id' => $id]);
    }

    public function deleteContact($id, $contactKey)
    {
        $customer = Customer::find($id);
        $contacts = json_decode($customer['contacts'],true);
        unset($contacts[$contactKey]);
        $customer['contacts'] = json_encode($contacts,true);
        $customer->save();
        return redirect()->route('customers.contacts', ['id' => $id]);
    }

}
