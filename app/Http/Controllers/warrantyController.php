<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Warranty;
use App\Franchise;
use App\Address;

use Illuminate\Http\Request;

class warrantyController extends Controller
{
    //
    //  Warranty Listing
    //

    public function index()
    {
        //Table Data
        $data = Warranty::all();

        //Data Manipulation
        foreach($data as $key => $row){
            $row['DT_RowId'] = $row['id'];
            $row['edit'] = "<button class='btn yellow edit'>Edit</button><button class='btn blue info'>Info</button>";
            $row['date'] = $row['created_at']->year."-".$row['created_at']->month."-".$row['created_at']->day;
            $row['date'] = date('M d, Y', strtotime($row['date']));
            if($row['type'] === "dmGutter"){
                $row['type'] = "Duck Matte Gutter";
            }
            if($row['type'] === "dmSiding"){
                $row['type'] = "Duck Matte Siding";
            }
            if($row['type'] === "pvcGutter"){
                $row['type'] = "PVC Gutter";
            }
            if($row['type'] === "pvcGutter"){
                $row['type'] = "PVC Siding";
            }
            if($row['type'] === "roofing"){
                $row['type'] = "Roofing";
            }
            $customer = Customer::find($row['customer_id']);
            $row['customer'] = $customer->name;
        }

        //Table Column Grab
        $columns[] = (object) array('data' => 'customer');
        $columns[] = (object) array('data' => 'type');
        $columns[] = (object) array('data' => 'date');
        $columns[] = (object) array('data' => 'edit');

        //Table Headers
        $headers = array('Customer','Type','Created','Functions');

        $pageName = "Warranty Listing";

        //View
        return view('listingWarranty', compact('data', 'headers', 'columns', 'pageName'));
    }

    //
    //  Warranty Creation Functions
    //

    //Show the first form for creating a new Warranty.
    public function create()
    {
        return view ('warranty');
    }

    //Show the first form for creating a new Warranty With Created Customer and Address
    public function warrantyNewCustomer($id,$address)
    {
        return view('warranty',compact('id','address'));
    }

    //Customer Search
    public function warrantyCustomer()
    {
        return view('warranty.customer');
    }

    //Create Customer form
    public function warrantyCreateCustomer($redirect)
    {
        $franchises = Franchise::all();
        foreach($franchises as $franchise){
            $userInfo = $franchise->user()->get();
            $user;
            foreach ($userInfo as $row){
                $user = $row;
            }
            $franchise['name'] = $user['name'];
        }
        return view('forms.customerCreate',compact('franchises','redirect'))->render();
    }

    //Show addresses
    public function warrantyAddress($id)
    {
        $customer = Customer::find($id);

        $addresses = $customer->addresses()->get();

        foreach($addresses as $key => $address){
            $json = json_decode($address['data'],true);
            foreach($json as $datakey => $data){
                $address[$datakey] = $data;
            }
        }

        return view('warranty.address', compact('addresses'));
    }

    //Show Waranty Types
    public function warrantyType()
    {

        return view('warranty.warranty');
    }

    //Show Warranty form
    public function warrantyForm($type)
    {
        if($type === 'dmGutter'){
            return view('forms.dmGutterCreate');
        }
        if($type === 'dmSiding'){
            return view('forms.dmSidingCreate');
        }
        if($type === 'pvcGutter'){
            return view('forms.pvcGutterCreate');
        }
        if($type === 'pvcSiding'){
            return view('forms.pvcSidingCreate');
        }
        if($type === 'roofing'){
            return view('forms.roofingCreate');
        }
    }



    //
    //  Warranty Creation
    //
    public function store(Request $request)
    {
        $warranty['customer_id'] = $request->customer;
        $warranty['type'] = $request->type;

        $data = $request->all();
        unset($data['_token'],$data['customer'],$data['type']);

        $warranty['data'] = json_encode($data,true);
        Warranty::create($warranty);
        return $request;
    }

    //
    //  Warranty Details
    //

    public function show($id)
    {
        $data = Warranty::find($id);

        $json = json_decode($data['date'],true);

        if($data['type'] === "dmGutter"){
            $data['type'] = "Duck Matte Gutter";
        }
        if($data['type'] === "dmSiding"){
            $data['type'] = "Duck Matte Siding";
        }
        if($data['type'] === "pvcGutter"){
            $data['type'] = "PVC Gutter";
        }
        if($data['type'] === "pvcGutter"){
            $data['type'] = "PVC Siding";
        }
        if($data['type'] === "roofing"){
            $data['type'] = "Roofing";
        }
        $customer = Customer::find($data['customer_id']);
        $data['customer'] = $customer->name;

        $data['date'] = $data['created_at']->year."-".$data['created_at']->month."-".$data['created_at']->day;
        $data['date'] = date('M d, Y', strtotime($data['date']));

        $route = "/warranties/".$id;

        $pageName = "Customer Details";

        $type = 'warranty';

        $baseHref = "/warranties/".$id;

        // View
        return view('warranty.details', compact('data', 'pageName','route','type','baseHref'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Warranty::find($id);
        $user->delete();
        return redirect()->route('warranties.index');
    }
}
