<?php

namespace App\Http\Controllers;
use App\Project;
use App\Customer;
use App\Address;
use App\Warranty;

use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;

class projectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $session = session('project');
        $project = [];

        if($session['config']['customer'] === "existing"){
            $customerID = $session['customer']['id'];
        }
        if($session['config']['customer'] === "new"){
            $customerSession = $session['customer'];
            $temp = $customerSession['contact']['fName'].'-'.$customerSession['contact']['lName'];
            $contact[$temp]['fName'] = $customerSession['contact']['fName'];
            $contact[$temp]['lName'] = $customerSession['contact']['lName'];
            $contact[$temp]['email'] = $customerSession['contact']['email'];
            $contact[$temp]['phone'] = $customerSession['contact']['phone'];
            $contact[$temp]['type'] = "Primary";

            $customer['contacts'] = json_encode($contact);

            $customer['name'] = $customerSession['name'];

            $customer['franchise_id'] = $customerSession['franchise'];

            $customerID = Customer::create($customer)->id;

            $address['address'] = $customerSession['address']['street'];
            $address['city'] = $customerSession['address']['city'];
            $address['state'] = $customerSession['address']['state'];
            $address['zip'] = $customerSession['address']['zip'];
            $address['county'] = $customerSession['address']['county'];
            $address['type'] = 'M';
            $main['data'] = json_encode($address,true);
            $main['customer_id'] = $customerID;

            $addressID = Address::create($main)->id;
        }

        if($session['config']['address'] === "existing"){
            $addressID = $session['address']['id'];
        }

        if($session['config']['address'] === "new"){

            $address['address'] = $customerSession['address']['street'];
            $address['city'] = $customerSession['address']['city'];
            $address['state'] = $customerSession['address']['state'];
            $address['zip'] = $customerSession['address']['zip'];
            $address['county'] = $customerSession['address']['county'];
            $address['type'] = 'M';
            $main['data'] = json_encode($address,true);
            $main['customer_id'] = $customerID;

            $addressID = Address::create($main)->id;
        }

        if($session['config']['billing'] === "existing"){
            if($session['config']['billingAddress'] === "same"){
                $billingID = $addressID;
            }
            if($session['config']['billingAddress'] === "different"){
                $billingID = $session['billing']['id'];
            }
        }
        if($session['config']['billing'] === "new"){
            if($session['config']['billingAddress'] === "same"){
                $billingID = $addressID;
            }
            if($session['config']['billingAddress'] === "different"){
                $address['address'] = $customerSession['billing']['street'];
                $address['city'] = $customerSession['billing']['city'];
                $address['state'] = $customerSession['billing']['state'];
                $address['zip'] = $customerSession['billing']['zip'];
                $address['county'] = $customerSession['billing']['county'];
                $address['type'] = 'M';
                $main['data'] = json_encode($address,true);
                $main['customer_id'] = $customerID;billing;

                $billingID = Address::create($main)->id;
            }
        }

        $project['customer_id'] = $customerID;
        $project['address_id'] = $addressID;
        $project['billing_id'] = $billingID;


        $projectID = Project::create($project)->id;

        foreach($session['warranty'] as $key => $value){
            $warranty['customer_id'] = $customerID;
            $warranty['project_id'] = $projectID;
            $warranty['type'] = $key;

            $data = $value;
            unset($data['customer'],$data['type']);

            $warranty['data'] = json_encode($data,true);
            Warranty::create($warranty);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sessionAccess()
    {
        return session('project');
    }

    public function projectReview()
    {
        $session = session('project');
        $data = [];
        $data['config'] = $session['config'];

        if($session['config']['customer'] === "existing"){
            $customerID = $session['customer']['id'];
            $customer = Customer::find($customerID);
            $json = json_decode($customer['contacts'],true);
            $customer['contacts'] = $json;
            $data['customer'] = $customer;
        }

        if($session['config']['customer'] === "new"){
            $data['customer'] = $session['customer'];

            $data['customer']['contacts'] = $session['customer']['contact'];
        }

        if($session['config']['address'] === "existing"){
            $addressID = $session['address']['id'];
            $address = Address::find($addressID);
            $json = json_decode($address['data'],true);
            $data['customer']['address'] = $json;
        }

        if($session['config']['address'] === "new"){
            $data['address'] = $session['customer']['address'];
        }
        if(isset($session['config']['billing'])){

            if($session['config']['billing'] === "existing"){
                $addressID = $session['billing']['id'];
                $data['billing'] = Address::find($addressID);
            }

            if($session['config']['billing'] === "new"){
                $data['billing'] = $session['customer']['billing'];
            }
        }

        foreach($session['types'] as $type){
            if($type['type'] === 'dmGutter'){
                $data['warrantys']['dmGutter'] = $session['warranty']['dmGutter'];
            }
            if($type['type'] === 'dmSiding'){
                $data['warrantys']['dmSiding'] = $session['warranty']['dmSiding'];
            }
            if($type['type'] === 'pvcGutter'){
                $data['warrantys']['pvcGutter'] = $session['warranty']['pvcGutter'];
            }
            if($type['type'] === 'pvcSiding'){
                $data['warrantys']['pvcSiding'] = $session['warranty']['pvcSiding'];
            }
            if($type['type'] === 'roofing'){
                $data['warrantys']['roofing'] = $session['warranty']['roofing'];
            }
        }

        return view('projects.review', compact('data'));

    }

    public function sessionStorage(Request $request)
    {
        if($request->type === "removeWarranty"){
            $warrantyType = $request->warranty;
            $warrantyType = 'project.warranty.'.$warrantyType;
            session()->forget($warrantyType);
            $types = session('project.types');
            foreach($types as $key => $type){
                if($type['type'] === $request->warranty){
                    unset($types[$key]);
                }
            }
            session(['project.types' => $types]);
        }

        if($request->type === "newCustomer"){
            session()->forget('project');
            session(['project.config.customer' => 'new']);
            session(['project.config.address' => 'new']);
            session(['project.config.billing' => 'new']);
            session(['project.config.billingAddress' => 'same']);

            session(['project.customer.name' => $request->name]);
            session(['project.customer.franchise' => $request->franchise]);

            session(['project.customer.contact.fName' => $request->fName]);
            session(['project.customer.contact.lName' => $request->lName]);
            session(['project.customer.contact.email' => $request->email]);
            session(['project.customer.contact.phone' => $request->phone]);

            session(['project.customer.address.street' => $request->address]);
            session(['project.customer.address.city' => $request->city]);
            session(['project.customer.address.state' => $request->state]);
            session(['project.customer.address.zip' => $request->zip]);
            session(['project.customer.address.county' => $request->county]);

            if($request->billing === "yes"){
                session(['project.customer.billing.street' => $request->address]);
                session(['project.customer.billing.city' => $request->city]);
                session(['project.customer.billing.state' => $request->state]);
                session(['project.customer.billing.zip' => $request->zip]);
                session(['project.customer.billing.county' => $request->county]);
            }

        }

        if($request->type === "existingCustomer"){
            session()->forget('project');
            session(['project.config.customer' => 'existing']);
            session(['project.customer.id' => $request->id]);
        }

        if($request->type === "newAddress"){
            session()->forget('project.customer.address');
            session(['project.config.address' => 'new']);
            if($request->billing === "yes"){
                session(['project.config.billing' => 'new']);
                session(['project.config.billingAddress' => 'same']);

                session(['project.customer.billing.street' => $request->address]);
                session(['project.customer.billing.city' => $request->city]);
                session(['project.customer.billing.state' => $request->state]);
                session(['project.customer.billing.zip' => $request->zip]);
                session(['project.customer.billing.county' => $request->county]);
            }

            session(['project.customer.address.street' => $request->address]);
            session(['project.customer.address.city' => $request->city]);
            session(['project.customer.address.state' => $request->state]);
            session(['project.customer.address.zip' => $request->zip]);
            session(['project.customer.address.county' => $request->county]);

        }

        if($request->type === "existingAddress"){
            session()->forget('project.address');
            session()->forget('project.customer.address');
            session(['project.config.address' => 'existing']);
            session(['project.address.id' => $request->id]);

            if($request->billing === "yes"){
                session(['project.config.billing' => 'existing']);
                session(['project.config.billingAddress' => 'same']);

                session(['project.billing.id' => $request->id]);
            }
        }

        if($request->type === "newBilling"){
            session()->forget('project.customer.billing');
            session(['project.config.billing' => 'new']);
            session(['project.config.billingAddress' => 'different']);

            session(['project.customer.billing.street' => $request->address]);
            session(['project.customer.billing.city' => $request->city]);
            session(['project.customer.billing.state' => $request->state]);
            session(['project.customer.billing.zip' => $request->zip]);
            session(['project.customer.billing.county' => $request->county]);

        }

        if($request->type === "existingBilling"){
            session()->forget('project.billing');
            session()->forget('project.customer.billing');
            session(['project.config.billingAddress' => 'different']);
            session(['project.config.billing' => 'existing']);
            session(['project.billing.id' => $request->id]);
        }

        if($request->type === "dmGutter"){
            session()->forget('project.warranty.dmGutter');

            $types = session('project.types');
            $array = array('type'=>'dmGutter');
            array_push($types,$array);
            session(['project.types' => $types]);

            session(['project.warranty.dmGutter.sales' => $request->consultant]);
            session(['project.warranty.dmGutter.installer' => $request->Installer]);
            session(['project.warranty.dmGutter.date' => $request->date]);
            session(['project.warranty.dmGutter.style' => $request->style]);
            session(['project.warranty.dmGutter.color' => $request->color]);
            session(['project.warranty.dmGutter.quantity' => $request->quantity]);
            session(['project.warranty.dmGutter.price' => $request->price]);
            session(['project.warranty.dmGutter.notes' => $request->notes]);

        }

        if($request->type === "pvcGutter"){
            session()->forget('project.warranty.pvcGutter');

            $types = session('project.types');
            $array = array('type'=>'pvcGutter');
            array_push($types,$array);
            session(['project.types' => $types]);

            session(['project.warranty.pvcGutter.sales' => $request->consultant]);
            session(['project.warranty.pvcGutter.installer' => $request->Installer]);
            session(['project.warranty.pvcGutter.date' => $request->date]);
            session(['project.warranty.pvcGutter.style' => $request->style]);
            session(['project.warranty.pvcGutter.color' => $request->color]);
            session(['project.warranty.pvcGutter.quantity' => $request->quantity]);
            session(['project.warranty.pvcGutter.price' => $request->price]);
            session(['project.warranty.pvcGutter.notes' => $request->notes]);

        }

        if($request->type === "dmSiding"){
            session()->forget('project.warranty.dmSiding');

            $types = session('project.types');
            $array = array('type'=>'dmSiding');
            array_push($types,$array);
            session(['project.types' => $types]);

            session(['project.warranty.dmSiding.sales' => $request->consultant]);
            session(['project.warranty.dmSiding.installer' => $request->Installer]);
            session(['project.warranty.dmSiding.date' => $request->date]);
            session(['project.warranty.dmSiding.size' => $request->size]);
            session(['project.warranty.dmSiding.style' => $request->sidingStyle]);
            session(['project.warranty.dmSiding.color' => $request->sidingColor1]);
            session(['project.warranty.dmSiding.color2' => $request->sidingColor2]);
            session(['project.warranty.dmSiding.quantity' => $request->quantity]);

            session(['project.warranty.dmSiding.wrap' => $request->wrapColor]);

            session(['project.warranty.dmSiding.soffit.style' => $request->soffitStyle]);
            session(['project.warranty.dmSiding.soffit.color' => $request->soffitColor]);
            session(['project.warranty.dmSiding.soffit.quantity' => $request->soffitQuantity]);

            session(['project.warranty.dmSiding.fascia.style' => $request->fasciaStyle]);
            session(['project.warranty.dmSiding.fascia.color' => $request->fasciaColor]);
            session(['project.warranty.dmSiding.fascia.quantity' => $request->fasciaQuantity]);

            session(['project.warranty.dmSiding.price' => $request->price]);
            session(['project.warranty.dmSiding.notes' => $request->notes]);

        }

        if($request->type === "pvcSiding"){
            session()->forget('project.warranty.pvcSiding');

            $types = session('project.types');
            $array = array('type'=>'pvcSiding');
            array_push($types,$array);
            session(['project.types' => $types]);

            session(['project.warranty.pvcSiding.sales' => $request->consultant]);
            session(['project.warranty.pvcSiding.installer' => $request->Installer]);
            session(['project.warranty.pvcSiding.date' => $request->date]);
            session(['project.warranty.pvcSiding.size' => $request->size]);
            session(['project.warranty.pvcSiding.style' => $request->sidingStyle]);
            session(['project.warranty.pvcSiding.color' => $request->sidingColor1]);
            session(['project.warranty.pvcSiding.color2' => $request->sidingColor2]);
            session(['project.warranty.pvcSiding.quantity' => $request->quantity]);

            session(['project.warranty.pvcSiding.wrap' => $request->wrapColor]);

            session(['project.warranty.pvcSiding.soffit.style' => $request->soffitStyle]);
            session(['project.warranty.pvcSiding.soffit.color' => $request->soffitColor]);
            session(['project.warranty.pvcSiding.soffit.quantity' => $request->soffitQuantity]);

            session(['project.warranty.pvcSiding.fascia.style' => $request->fasciaStyle]);
            session(['project.warranty.pvcSiding.fascia.color' => $request->fasciaColor]);
            session(['project.warranty.pvcSiding.fascia.quantity' => $request->fasciaQuantity]);

            session(['project.warranty.pvcSiding.price' => $request->price]);
            session(['project.warranty.pvcSiding.notes' => $request->notes]);

        }

        if($request->type === "roofing"){
            session()->forget('project.warranty.roofing');

            $types = session('project.types');
            $array = array('type'=>'roofing');
            array_push($types,$array);
            session(['project.types' => $types]);

            session(['project.warranty.roofing.sales' => $request->consultant]);
            session(['project.warranty.roofing.installer' => $request->Installer]);
            session(['project.warranty.roofing.date' => $request->date]);
            session(['project.warranty.roofing.material' => $request->material]);
            session(['project.warranty.roofing.color' => $request->color1]);
            session(['project.warranty.roofing.quantity' => $request->quantity]);

            session(['project.warranty.roofing.price' => $request->price]);
            session(['project.warranty.roofing.notes' => $request->notes]);

        }

    }
}
