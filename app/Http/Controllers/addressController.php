<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;
use App\Address;

class addressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function json($id)
    {
        $customer = Customer::find($id);

        $addresses = $customer->addresses()->get();

        foreach($addresses as $address){
            $address['data'] = json_decode($address['data'],true);
            $address['DT_RowId'] = $address['id'];
        }

        return $addresses;
    }

    public function index($id)
    {
        $states = array(
           "AL"=>"Alabama",
           "AK"=>"Alaska",
           "AZ"=>"Arizona",
           "AR"=>"Arkansas",
           "CA"=>"California",
           "CO"=>"Colorado",
           "CT"=>"Connecticut",
           "DE"=>"Delaware",
           "DC"=>"District Of Columbia",
           "FL"=>"Florida",
           "GA"=>"Georgia",
           "HI"=>"Hawaii",
           "ID"=>"Idaho",
           "IL"=>"Illinois",
           "IN"=>"Indiana",
           "IA"=>"Iowa",
           "KS"=>"Kansas",
           "KY"=>"Kentucky",
           "LA"=>"Louisiana",
           "ME"=>"Maine",
           "MD"=>"Maryland",
           "MA"=>"Massachusetts",
           "MI"=>"Michigan",
           "MN"=>"Minnesota",
           "MS"=>"Mississippi",
           "MO"=>"Missouri",
           "MT"=>"Montana",
           "NE"=>"Nebraska",
           "NV"=>"Nevada",
           "NH"=>"New Hampshire",
           "NJ"=>"New Jersey",
           "NM"=>"New Mexico",
           "NY"=>"New York",
           "NC"=>"North Carolina",
        "ND"=>"North Dakota",
   "OH"=>"Ohio",
   "OK"=>"Oklahoma",
   "OR"=>"Oregon",
   "PA"=>"Pennsylvania",
   "RI"=>"Rhode Island",
        "SC"=>"South Carolina",
"SD"=>"South Dakota",
"TN"=>"Tennessee",
"TX"=>"Texas",
"UT"=>"Utah",
"VT"=>"Vermont",
"VA"=>"Virginia",
"WA"=>"Washington",
"WV"=>"West Virginia",
"WI"=>"Wisconsin",
"WY"=>"Wyoming"
);
$customer = Customer::find($id);

$addresses = $customer->addresses()->get();

$data;

foreach($addresses as $key => $address){
$json = json_decode($address['data'],true);
$json['id'] = $address['id'];
$data[$key] = $json;
}

$route = "/customers/".$id;

$pageName = "Customer Details";

$type = 'customer';

$baseHref = "/customers/".$id;


// View
return view('customers.addresses', compact('pageName','route','type','baseHref','data','states'));
}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addresses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $address['customer_id'] = $id;
        $json['address'] = $request->address;
        $json['city'] = $request->city;
        $json['state'] = $request->state;
        $json['zip'] = $request->zip;
        $json['county'] = $request->county;
        $json['type'] = 'S';
        $address['data'] = json_encode($json,true);
        Address::create($address);
        return redirect()->route('customers.addresses', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $addressID = $request->key;
        $street = $request->address;
        $city = $request->city;
        $state = $request->state;
        $zip = $request->zip;
        $county = $request->county;
        $address = Address::find($addressID);
        $json = json_decode($address['data']);

        $json->address = $street;
        $json->city = $city;
        $json->state = $state;
        $json->zip = $zip;
        $json->county = $county;

        $address['data'] = json_encode($json);
        $address->save();

        return redirect()->route('customers.addresses', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($key)
    {
        $address = Address::find($key);
        $id = $address['customer_id'];
        $address->delete();
        return redirect()->route('customers.addresses', ['id' => $id]);
    }

    public function masterAddress(Request $request)
    {
        $address = Address::find($request->key);
        $id = $address->customer_id;
        $addresses = Address::all();
        foreach($addresses as $record){
            $json = json_decode($record['data'],true);
            $json['type'] = 'S';
            $record['data'] = json_encode($json,true);
            $record->save();
        }
        $json = json_decode($address['data'],true);
        $json['type'] = 'M';
        $address['data'] = json_encode($json,true);
        $address->save();
        return redirect()->route('customers.addresses', ['id' => $id]);
    }
}
