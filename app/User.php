<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Franchise as Franchise;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function hasType($type)
    {
        return $this->type == $type;
    }

    public function getFranchise()
    {
        $user = $this->id;
        $franchise = Franchise::where('user_id', '=', $user)->get()->toArray();
        return $franchise[0]['id'];
    }
}
