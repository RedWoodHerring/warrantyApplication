<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    protected $fillable = [
        'customer_id','data','type','created_at','project_id'
    ];
}
