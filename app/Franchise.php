<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Franchise extends Model
{
    protected $fillable = [
        'data','user_id'
    ];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
