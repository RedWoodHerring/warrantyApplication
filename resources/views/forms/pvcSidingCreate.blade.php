<input name="type" type="hidden" value="pvcSiding"></input>

<div class="form-group{{ $errors->has('consultant') ? ' has-error' : '' }}">
  <label for="consultant" class="col-md-4 control-label">Sales Consultant</label>

  <div class="col-md-6">
      <input id="consultant" type="text" class="form-control" name="consultant" value="{{ old('consultant') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('Installer') ? ' has-error' : '' }}">
  <label for="Installer" class="col-md-4 control-label">Installer's Name</label>

  <div class="col-md-6">
      <input id="Installer" type="text" class="form-control" name="Installer" value="{{ old('Installer') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
  <label for="date" class="col-md-4 control-label">Installation Date</label>

  <div class="col-md-6">
      <input id="date" type="date" class="form-control" name="date" value="{{ old('date') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('size') ? ' has-error' : '' }}">
  <label for="size" class="col-md-4 control-label">Siding Size</label>

  <div class="col-md-6">
      <select id="size" class="form-control" name="size">
          <option>Select Size</option>
          <option value="2">2"</option>
          <option value="3">3"</option>
          <option value="4">4"</option>
          <option value="5">5"</option>
          <option value="6">6"</option>
          <option value="8">8"</option>
          <option value="10">10"</option>
          <option value="12">12"</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('sidingStyle') ? ' has-error' : '' }}">
  <label for="sidingStyle" class="col-md-4 control-label">Siding Style</label>

  <div class="col-md-6">
      <select id="sidingStyle" class="form-control" name="sidingStyle">
          <option>Select Style</option>
          <option value="log">Log</option>
          <option value="boardBatten">Board & Batten</option>
          <option value="boardBattenReverse">Reverse Board & Batten</option>
          <option value="flat">Flat</option>
          <option value="colonial">Colonial</option>
          <option value="luxemberg">Luxemberg</option>
          <option value="colonialLux">Colonial Lux</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('sidingColor1') ? ' has-error' : '' }}">
  <label for="sidingColor1" class="col-md-4 control-label">Siding Color</label>

  <div class="col-md-6">
      <select id="sidingColor1" class="form-control" name="sidingColor1">
        <option>Select Color</option>
        <option value="cinnamon">Cinnamon</option>
		<option value="cedarwood">Cedarwood</option>
		<option value="rusticBrown">Rustic Brown</option>
		<option value="yellowGlow">Yellow Glow</option>
		<option value="sherwoodGreen">Sherwood Green</option>
		<option value="slateBlue">Slate Blue</option>
		<option value="heritageBlue">Heritage Blue</option>
		<option value="springGreen">Spring Green</option>
		<option value="charcoalGray">Charcoal Gray</option>
		<option value="williamsburgGray">Williamsburg Gray</option>
		<option value="weatheredCedar">Weathered Cedar</option>
		<option value="sandBeige">Sand Beige</option>
		<option value="wickertone">Wickertone</option>
		<option value="desertTone">Desert Tone</option>
		<option value="colonialWhite">Colonial White</option>
		<option value="pearl">Pearl</option>
		<option value="ivory">Ivory</option>

      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('sidingColor2') ? ' has-error' : '' }}">
  <label for="sidingColor2" class="col-md-4 control-label">Siding Color 2</label>

  <div class="col-md-6">
      <select id="sidingColor2" class="form-control" name="sidingColor2">
        <option>Select Color</option>
        <option value="cinnamon">Cinnamon</option>
		<option value="cedarwood">Cedarwood</option>
		<option value="rusticBrown">Rustic Brown</option>
		<option value="yellowGlow">Yellow Glow</option>
		<option value="sherwoodGreen">Sherwood Green</option>
		<option value="slateBlue">Slate Blue</option>
		<option value="heritageBlue">Heritage Blue</option>
		<option value="springGreen">Spring Green</option>
		<option value="charcoalGray">Charcoal Gray</option>
		<option value="williamsburgGray">Williamsburg Gray</option>
		<option value="weatheredCedar">Weathered Cedar</option>
		<option value="sandBeige">Sand Beige</option>
		<option value="wickertone">Wickertone</option>
		<option value="desertTone">Desert Tone</option>
		<option value="colonialWhite">Colonial White</option>
		<option value="pearl">Pearl</option>
		<option value="ivory">Ivory</option>

      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
  <label for="quantity" class="col-md-4 control-label">Siding Quantity (# sqs.)</label>

  <div class="col-md-6">
      <input id="quantity" type="number" class="form-control" name="quantity" value="{{ old('quantity') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('wrapColor') ? ' has-error' : '' }}">
  <label for="wrapColor" class="col-md-4 control-label">Wrap Color</label>

  <div class="col-md-6">
      <select id="wrapColor" class="form-control" name="wrapColor">
        <option>Select Color</option>
        <option value="cinnamon">Cinnamon</option>
		<option value="cedarwood">Cedarwood</option>
		<option value="rusticBrown">Rustic Brown</option>
		<option value="yellowGlow">Yellow Glow</option>
		<option value="sherwoodGreen">Sherwood Green</option>
		<option value="slateBlue">Slate Blue</option>
		<option value="heritageBlue">Heritage Blue</option>
		<option value="springGreen">Spring Green</option>
		<option value="charcoalGray">Charcoal Gray</option>
		<option value="williamsburgGray">Williamsburg Gray</option>
		<option value="weatheredCedar">Weathered Cedar</option>
		<option value="sandBeige">Sand Beige</option>
		<option value="wickertone">Wickertone</option>
		<option value="desertTone">Desert Tone</option>
		<option value="colonialWhite">Colonial White</option>
		<option value="pearl">Pearl</option>
		<option value="ivory">Ivory</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('soffitStyle') ? ' has-error' : '' }}">
  <label for="soffitStyle" class="col-md-4 control-label">Soffit Style</label>

  <div class="col-md-6">
      <select id="soffitStyle" class="form-control" name="soffitStyle">
          <option>Select Style</option>
          <option value="steel">Steel</option>
          <option value="other">Other</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('soffitColor') ? ' has-error' : '' }}">
  <label for="soffitColor" class="col-md-4 control-label">Soffit Color</label>

  <div class="col-md-6">
      <select id="soffitColor" class="form-control" name="soffitColor">
        <option>Select Color</option>
        <option value="cinnamon">Cinnamon</option>
		<option value="cedarwood">Cedarwood</option>
		<option value="rusticBrown">Rustic Brown</option>
		<option value="yellowGlow">Yellow Glow</option>
		<option value="sherwoodGreen">Sherwood Green</option>
		<option value="slateBlue">Slate Blue</option>
		<option value="heritageBlue">Heritage Blue</option>
		<option value="springGreen">Spring Green</option>
		<option value="charcoalGray">Charcoal Gray</option>
		<option value="williamsburgGray">Williamsburg Gray</option>
		<option value="weatheredCedar">Weathered Cedar</option>
		<option value="sandBeige">Sand Beige</option>
		<option value="wickertone">Wickertone</option>
		<option value="desertTone">Desert Tone</option>
		<option value="colonialWhite">Colonial White</option>
		<option value="pearl">Pearl</option>
		<option value="ivory">Ivory</option>

      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('soffitQuantity') ? ' has-error' : '' }}">
  <label for="soffitQuantity" class="col-md-4 control-label">Soffit Quantity (# ft.)</label>

  <div class="col-md-6">
      <input id="soffitQuantity" type="number" class="form-control" name="soffitQuantity" value="{{ old('soffitQuantity') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('fasciaStyle') ? ' has-error' : '' }}">
  <label for="fasciaStyle" class="col-md-4 control-label">Soffit Style</label>

  <div class="col-md-6">
      <select id="fasciaStyle" class="form-control" name="fasciaStyle">
          <option>Select Style</option>
          <option value="steel">Steel</option>
          <option value="other">Other</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('fasciaColor') ? ' has-error' : '' }}">
  <label for="fasciaColor" class="col-md-4 control-label">Fascia Color</label>

  <div class="col-md-6">
      <select id="fasciaColor" class="form-control" name="fasciaColor">
        <option>Select Color</option>
        <option value="cinnamon">Cinnamon</option>
		<option value="cedarwood">Cedarwood</option>
		<option value="rusticBrown">Rustic Brown</option>
		<option value="yellowGlow">Yellow Glow</option>
		<option value="sherwoodGreen">Sherwood Green</option>
		<option value="slateBlue">Slate Blue</option>
		<option value="heritageBlue">Heritage Blue</option>
		<option value="springGreen">Spring Green</option>
		<option value="charcoalGray">Charcoal Gray</option>
		<option value="williamsburgGray">Williamsburg Gray</option>
		<option value="weatheredCedar">Weathered Cedar</option>
		<option value="sandBeige">Sand Beige</option>
		<option value="wickertone">Wickertone</option>
		<option value="desertTone">Desert Tone</option>
		<option value="colonialWhite">Colonial White</option>
		<option value="pearl">Pearl</option>
		<option value="ivory">Ivory</option>

      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('fasciaQuantity') ? ' has-error' : '' }}">
  <label for="fasciaQuantity" class="col-md-4 control-label">Fascia Quantity (# ft.)</label>

  <div class="col-md-6">
      <input id="fasciaQuantity" type="number" class="form-control" name="fasciaQuantity" value="{{ old('fasciaQuantity') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
  <label for="price" class="col-md-4 control-label">Customer's Purchase Price</label>

  <div class="col-md-6">
      <input id="price" type="number" class="form-control" name="price" value="{{ old('price') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
  <label for="notes" class="col-md-4 control-label">Additional Information - optional: (this will show up in the notes section of your warranty)</label>

  <div class="col-md-6">
      <textarea id="notes" class="form-control" name="notes"></textarea>

  </div>
</div>
