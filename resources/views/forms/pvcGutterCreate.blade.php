<input name="type" type="hidden" value="pvcGutter"></input>

<div class="form-group{{ $errors->has('consultant') ? ' has-error' : '' }}">
  <label for="consultant" class="col-md-4 control-label">Sales Consultant</label>

  <div class="col-md-6">
      <input id="consultant" type="text" class="form-control" name="consultant" value="{{ old('consultant') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('Installer') ? ' has-error' : '' }}">
  <label for="Installer" class="col-md-4 control-label">Installer's Name</label>

  <div class="col-md-6">
      <input id="Installer" type="text" class="form-control" name="Installer" value="{{ old('Installer') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
  <label for="date" class="col-md-4 control-label">Installation Date</label>

  <div class="col-md-6">
      <input id="date" type="date" class="form-control" name="date" value="{{ old('date') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('style') ? ' has-error' : '' }}">
  <label for="style" class="col-md-4 control-label">Gutter Style</label>

  <div class="col-md-6">
      <select id="style" class="form-control" name="style">
          <option value="">Select Style</option>
          <option value="5kStyle">5" K-Style</option>
          <option value="6kStyle">6" K-Style</option>
          <option value="5fStyle">5" Fascia Style</option>
          <option value="6fStyle">6" Fascia Style</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
  <label for="color" class="col-md-4 control-label">Gutter Color</label>

  <div class="col-md-6">
      <select id="color" class="form-control" name="color">
          <option value="" selected=""></option>
          <option value="charcoalGray">Charcoal Gray</option>
        <option value="cinnamon">Cinnamon</option>
        <option value="cedarwood">Cedarwood</option>
        <option value="colonialWhite">Colonial White</option>
        <option value="desertTone">Desert Tone</option>
        <option value="heritageBlue">Heritage Blue</option>
        <option value="ivory">Ivory</option>
        <option value="pearl">Pearl</option>
        <option value="rusticBrown">Rustic Brown</option>
        <option value="sandBeige">Sand Beige</option>
        <option value="sherwoodGreen">Sherwood Green</option>
        <option value="slateBlue">Slate Blue</option>
        <option value="springGreen">Spring Green</option>
        <option value="weatheredCedar">Weathered Cedar</option>
        <option value="wickertone">Wickertone</option>
        <option value="williamsburgGray">Williamsburg Gray</option>
        <option value="yellowGlow">Yellow Glow</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
  <label for="quantity" class="col-md-4 control-label">Gutter Quantity (# ft.)</label>

  <div class="col-md-6">
      <input id="quantity" type="number" class="form-control" name="quantity" value="{{ old('quantity') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
  <label for="price" class="col-md-4 control-label">Customer's Purchase Price</label>

  <div class="col-md-6">
      <input id="price" type="number" class="form-control" name="price" value="{{ old('price') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
  <label for="notes" class="col-md-4 control-label">Additional Information - optional: (this will show up in the notes section of your warranty)</label>

  <div class="col-md-6">
      <textarea id="notes" class="form-control" name="notes"></textarea>

  </div>
</div>
