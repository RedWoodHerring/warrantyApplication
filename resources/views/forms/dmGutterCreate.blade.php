<input name="type" type="hidden" value="dmGutter"></input>

<div class="form-group{{ $errors->has('consultant') ? ' has-error' : '' }}">
  <label for="consultant" class="col-md-4 control-label">Sales Consultant</label>

  <div class="col-md-6">
      <input id="consultant" type="text" class="form-control" name="consultant" value="{{ old('consultant') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('Installer') ? ' has-error' : '' }}">
  <label for="Installer" class="col-md-4 control-label">Installer's Name</label>

  <div class="col-md-6">
      <input id="Installer" type="text" class="form-control" name="Installer" value="{{ old('Installer') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
  <label for="date" class="col-md-4 control-label">Installation Date</label>

  <div class="col-md-6">
      <input id="date" type="date" class="form-control" name="date" value="{{ old('date') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('style') ? ' has-error' : '' }}">
  <label for="style" class="col-md-4 control-label">Gutter Style</label>

  <div class="col-md-6">
      <select id="style" class="form-control" name="style">
          <option value="">Select Style</option>
          <option value="5kStyle">5" K-Style</option>
          <option value="6kStyle">6" K-Style</option>
          <option value="5fStyle">5" Fascia Style</option>
          <option value="6fStyle">6" Fascia Style</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
  <label for="color" class="col-md-4 control-label">Gutter Color</label>

  <div class="col-md-6">
      <select id="color" class="form-control" name="color">
        <option value="">Select Color</option>
        <option value="almondCream">Almond Cream</option>
        <option value="adobeCream">Adobe Cream</option>
        <option value="antiqueParchment">Antique Parchment</option>
        <option value="autumnBeige">Autumn Beige</option>
        <option value="black">Black</option>
        <option value="canyon">Canyon</option>
        <option value="canyonHD">Canyon HD</option>
        <option value="cedarwood">Cedarwood</option>
        <option value="cedarwoodHD">Cedarwood HD</option>
        <option value="charcoalGray">Charcoal Gray</option>
        <option value="classicBlue">Classic Blue</option>
        <option value="classicRed">Classic Red</option>
        <option value="claytone">Claytone</option>
        <option value="desertTone">Desert Tone</option>
        <option value="driftwoodGray">Driftwood Gray</option>
        <option value="glacierWhite">Glacier White</option>
        <option value="hartfordGreen">Hartford Green</option>
        <option value="mahogany">Mahogany</option>
        <option value="mahogany HD">Mahogany HD</option>
        <option value="pewter">Pewter</option>
        <option value="prismBordeaux">Prism - Bordeaux</option>
        <option value="prismOnyx">Prism - Onyx</option>
        <option value="prismSapphire">Prism - Sapphire</option>
        <option value="prismTuscan">Prism - Tuscan</option>
        <option value="royalBrown">Royal Brown</option>
        <option value="rusticBrown">Rustic Brown</option>
        <option value="sage">Sage</option>
        <option value="sandtone">Sandtone</option>
        <option value="statuaryBronze">Statuary Bronze</option>
        <option value="tTone">T-Tone</option>
        <option value="timber">Timber</option>
        <option value="wickertone">Wickertone</option>
        <option value="willow">Willow</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
  <label for="quantity" class="col-md-4 control-label">Gutter Quantity (# ft.)</label>

  <div class="col-md-6">
      <input id="quantity" type="number" class="form-control" name="quantity" value="{{ old('quantity') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
  <label for="price" class="col-md-4 control-label">Customer's Purchase Price</label>

  <div class="col-md-6">
      <input id="price" type="number" class="form-control" name="price" value="{{ old('price') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
  <label for="notes" class="col-md-4 control-label">Additional Information - optional: (this will show up in the notes section of your warranty)</label>

  <div class="col-md-6">
      <textarea id="notes" class="form-control" name="notes"></textarea>

  </div>
</div>
