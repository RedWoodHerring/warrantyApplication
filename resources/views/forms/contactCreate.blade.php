<div class="form-wrapper">
  <form id="main-form" class="" method="POST" action="{{route('customers.contacts.store', ['id' => $id])}}">
      {{ csrf_field() }}

      <div class="form-group{{ $errors->has('fName') ? ' has-error' : '' }}">
          <label for="fName" class="col-md-4 control-label">First Name</label>

          <div class="col-md-6">
              <input id="fName" type="text" class="form-control" name="fName" value="{{ old('fName') }}" required autofocus>

          </div>
      </div>

      <div class="form-group{{ $errors->has('lName') ? ' has-error' : '' }}">
          <label for="lName" class="col-md-4 control-label">Last Name</label>

          <div class="col-md-6">
              <input id="lName" type="text" class="form-control" name="lName" value="{{ old('lName') }}" required autofocus>

          </div>
      </div>

      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <label for="email" class="col-md-4 control-label">Email</label>

          <div class="col-md-6">
              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

          </div>
      </div>

      <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
          <label for="phone" class="col-md-4 control-label">Phone</label>

          <div class="col-md-6">
              <input id="phone" type="number" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

          </div>
      </div>

      <div class="form-control">
          <button type="button" class="btn yellow cancel">Cancel</button>
          <button type="submit" form="main-form" class="btn green">Submit</button>
      </div>

  </form>
</div>
<script>
    $(".cancel").off("click").on("click", function(){
        location.reload();
    });
</script>
