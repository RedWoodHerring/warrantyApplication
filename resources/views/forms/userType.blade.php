<h3 class="box-header">New User Form</h3>
<div class="form-wrapper">
  <form>

      <div class="form-group">
          <label for="type" class="col-md-4 control-label">Account Type</label>

          <div class="col-md-6">
              <select id="type" class="form-control" name="type" required autofocus>
                <option value=''>Please Select A Type</option>
                <option value='C'>Customer</option>
                <option value='F'>Franchise</option>
                <option value='A'>Admin</option>
              </select>
          </div>

      </div>
  </form>

</div>
<div id="ajax-form" class="form-wrapper"></div>
<button type="button" class="btn yellow cancel">Cancel</button>
<button type="submit" form="main-form" class="btn green hidden">Submit</button>

<script>
  $("#type").change(function() {
  var type = $(this).val();
  if (type === "A") {
    $.ajax({
        method: "GET",
        url: window.location.pathname + "/typeCreate/" + type
      })
      .done(function(data) {
        $("button[type='submit']").removeClass("hidden");
        $("#ajax-form").html(data);
      });
  } else if (type === "F") {
    $.ajax({
        method: "GET",
        url: window.location.pathname + "/typeCreate/" + type
      })
      .done(function(data) {
        $("button[type='submit']").removeClass("hidden");
        $("#ajax-form").html(data);
      });
  } else {
    $("#ajax-form").html("");
    $("button[type='submit']").addClass("hidden");
  }
});
  $("#ajax-form").off("click").on("click", ".cancel", function(){
    $(".form-container").prev().removeClass("hidden");
    $(".form-container").removeClass("box");
    $(".form-container").html("");
  });
  $(".cancel").off("click").on("click", function(){
    $(".form-container").prev().removeClass("hidden");
    $(".form-container").removeClass("box");
    $(".form-container").html("");
  });
</script>
