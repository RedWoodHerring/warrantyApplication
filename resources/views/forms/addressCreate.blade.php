<div class="form-wrapper">
  <form id="main-form" class="" method="POST" action="{{route('customers.address.store', ['id' => $id])}}">
      {{ csrf_field() }}

      <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
          <label for="address" class="col-md-4 control-label">Address</label>

          <div class="col-md-6">
              <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus>

          </div>
      </div>

      <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
          <label for="city" class="col-md-4 control-label">City</label>

          <div class="col-md-6">
              <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" required autofocus>

          </div>
      </div>

      <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
          <label for="state" class="col-md-4 control-label">State</label>

          <div class="col-md-6">
              <select id="state" class="form-control" name="state" required autofocus>
                  <option value="AL">Alabama</option>
                  <option value="AK">Alaska</option>
                  <option value="AZ">Arizona</option>
                  <option value="AR">Arkansas</option>
                  <option value="CA">California</option>
                  <option value="CO">Colorado</option>
                  <option value="CT">Connecticut</option>
                  <option value="DE">Delaware</option>
                  <option value="DC">District Of Columbia</option>
                  <option value="FL">Florida</option>
                  <option value="GA">Georgia</option>
                  <option value="HI">Hawaii</option>
                  <option value="ID">Idaho</option>
                  <option value="IL">Illinois</option>
                  <option value="IN">Indiana</option>
                  <option value="IA">Iowa</option>
                  <option value="KS">Kansas</option>
                  <option value="KY">Kentucky</option>
                  <option value="LA">Louisiana</option>
                  <option value="ME">Maine</option>
                  <option value="MD">Maryland</option>
                  <option value="MA">Massachusetts</option>
                  <option value="MI">Michigan</option>
                  <option value="MN">Minnesota</option>
                  <option value="MS">Mississippi</option>
                  <option value="MO">Missouri</option>
                  <option value="MT">Montana</option>
                  <option value="NE">Nebraska</option>
                  <option value="NV">Nevada</option>
                  <option value="NH">New Hampshire</option>
                  <option value="NJ">New Jersey</option>
                  <option value="NM">New Mexico</option>
                  <option value="NY">New York</option>
                  <option value="NC">North Carolina</option>
                  <option value="ND">North Dakota</option>
                  <option value="OH">Ohio</option>
                  <option value="OK">Oklahoma</option>
                  <option value="OR">Oregon</option>
                  <option value="PA">Pennsylvania</option>
                  <option value="RI">Rhode Island</option>
                  <option value="SC">South Carolina</option>
                  <option value="SD">South Dakota</option>
                  <option value="TN">Tennessee</option>
                  <option value="TX">Texas</option>
                  <option value="UT">Utah</option>
                  <option value="VT">Vermont</option>
                  <option value="VA">Virginia</option>
                  <option value="WA">Washington</option>
                  <option value="WV">West Virginia</option>
                  <option value="WI">Wisconsin</option>
                  <option value="WY">Wyoming</option>
              </select>

          </div>
      </div>

      <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
          <label for="zip" class="col-md-4 control-label">Zip</label>

          <div class="col-md-6">
              <input id="zip" type="number" class="form-control" name="zip" value="{{ old('zip') }}" required autofocus>

          </div>
      </div>

      <div class="form-group{{ $errors->has('county') ? ' has-error' : '' }}">
          <label for="county" class="col-md-4 control-label">County</label>

          <div class="col-md-6">
              <input id="county" type="text" class="form-control" name="county" value="{{ old('county') }}" required autofocus>

          </div>
      </div>

      <div class="form-control">
          <button type="button" class="btn yellow cancel">Cancel</button>
          <button type="submit" form="main-form" class="btn green">Submit</button>
      </div>

  </form>
</div>
<script>
    $(".cancel").off("click").on("click", function(){
        location.reload();
    });
</script>
