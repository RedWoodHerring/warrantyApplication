<input name="type" type="hidden" value="roofing"></input>

<div class="form-group{{ $errors->has('consultant') ? ' has-error' : '' }}">
  <label for="consultant" class="col-md-4 control-label">Sales Consultant</label>

  <div class="col-md-6">
      <input id="consultant" type="text" class="form-control" name="consultant" value="{{ old('consultant') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('Installer') ? ' has-error' : '' }}">
  <label for="Installer" class="col-md-4 control-label">Installer's Name</label>

  <div class="col-md-6">
      <input id="Installer" type="text" class="form-control" name="Installer" value="{{ old('Installer') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
  <label for="date" class="col-md-4 control-label">Installation Date</label>

  <div class="col-md-6">
      <input id="date" type="date" class="form-control" name="date" value="{{ old('date') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('material') ? ' has-error' : '' }}">
  <label for="material" class="col-md-4 control-label">Roofing Material</label>

  <div class="col-md-6">
      <select id="material" class="form-control" name="material">
          <option>Select Size</option>
          <option value="singleShake">Single/Shake</option>
          <option value="slate">Slate</option>
          <option value="seamless">Seamless</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('color1') ? ' has-error' : '' }}">
  <label for="color1" class="col-md-4 control-label">1st Color</label>

  <div class="col-md-6">
      <select id="color1" class="form-control" name="color1">
          <option>Select Material First</option>
      </select>

  </div>
</div>

<div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
  <label for="quantity" class="col-md-4 control-label">Roofing Qty (# sqs.)</label>

  <div class="col-md-6">
      <input id="quantity" type="number" class="form-control" name="quantity" value="{{ old('quantity') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
  <label for="price" class="col-md-4 control-label">Customer's Purchase Price</label>

  <div class="col-md-6">
      <input id="price" type="number" class="form-control" name="price" value="{{ old('price') }}" required autofocus>

  </div>
</div>

<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
  <label for="notes" class="col-md-4 control-label">Additional Information - optional: (this will show up in the notes section of your warranty)</label>

  <div class="col-md-6">
      <textarea id="notes" class="form-control" name="notes"></textarea>

  </div>
</div>
<script>
$("#material").change(function(){
    if($(this).val() === "singleShake"){
        var html = "<option>Select Color</option><option value='sandtone'>Sandtone</option><option value='cedar'>Cedar</option><option value='pewter'>Pewter</option><option value='charcoalGray'>Charcoal Gray</option><option value='tTone'>T-Tone</option><option value='statuaryBronze'>Statuary Bronze</option><option value='royalBrown'>Royal Brown</option><option value='hartfordGreen'>Hartford Green</option><option value='copper'>Copper</option><option value='classicBlue'>Classic Blue</option><option value='classicRed'>Classic Red</option><option value='charcoalGrayBlend'>Enhanced: Charcoal Gray Blend</option><option value='classicRedBlend'>Enhanced: Classic Red Blend</option><option value='hartfordGreenBlend'>Enhanced: Hartford Green Blend</option><option value='royalBrownBlend'>Enhanced: Royal Brown Blend</option><option value='statuaryBronzeBlend'>Enhanced: Statuary Bronze Blend</option><option value='tToneBlend'>Enhanced: T-Tone Blend</option>";
    }
    if($(this).val() === "slate"){
        var html = "<option>Select Color</option><option value='stone'>Stone</option><option value='charcoalGray'>Charcoal Gray</option><option value='classicRed'>Classic Red</option><option value='hartfordGreen'>Hartford Green</option><option value='statuaryBronze'>Statuary Bronze</option><option value='charcoalGrayBlend'>Enhanced: Charcoal Gray Blend</option><option value='classicRedBlend'>Enhanced: Classic Red Blend</option><option value='royalBrownBlend'>Enhanced: Royal Brown Blend</option><option value='enhancedStatuaryBronze'>Enhanced: Statuary Bronze Blend</option><option value='stoneBlend'>Enhanced: Stone Blend</option><option value='tToneBlend'>Enhanced: T-Tone Blend</option>"
    }
    if($(this).val() === "seamless"){
        var html = "<option>Select Color</option><option value='hdHartfordGreen'>HD Hartford Green</option><option value='hdClassicRed'>HD Classic Red</option><option value='hdCharcoalGray'>HD Charcoal Gray</option><option value='hdTTone'>HD T-Tone</option><option value='hdCedar'>HD Cedar</option><option value='hdCopper'>HD Copper</option><option value='hdRoyalBrown'>HD Royal Brown</option>"
    }
    $("#color1").html(html);

});
</script>
