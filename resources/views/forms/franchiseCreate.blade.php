@if(!isset($type))
<h3 class="box-header">New Franchise Form</h3>
@endif
<div class="form-wrapper">
  <form id="main-form" class="" method="POST" action="{{ route('franchises.store') }}">
      {{ csrf_field() }}

      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="col-md-4 control-label">Franchise Name</label>

          <div class="col-md-6">
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
          <label for="username" class="col-md-4 control-label">User Name</label>

          <div class="col-md-6">
              <input id="username" class="form-control" name="username" value="{{ old('username') }}" required>

              @if ($errors->has('username'))
                  <span class="help-block">
                      <strong>{{ $errors->first('username') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <label for="password" class="col-md-4 control-label">Password</label>

          <div class="col-md-6">
              <input id="password" type="password" class="form-control" name="password" required>

              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group">
          <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

          <div class="col-md-6">
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
          </div>
      </div>



      <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
          <label for="address" class="col-md-4 control-label">Address</label>

          <div class="col-md-6">
              <input id="address" class="form-control" name="address" value="{{ old('address') }}" required>

              @if ($errors->has('address'))
                  <span class="help-block">
                      <strong>{{ $errors->first('address') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
          <label for="city" class="col-md-4 control-label">City</label>

          <div class="col-md-6">
              <input id="city" class="form-control" name="city" value="{{ old('city') }}" required>

              @if ($errors->has('city'))
                  <span class="help-block">
                      <strong>{{ $errors->first('city') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
          <label for="state" class="col-md-4 control-label">State</label>

          <div class="col-md-6">
              <select id="state" class="form-control" name="state" required>
                	<option value="AL">Alabama</option>
                	<option value="AK">Alaska</option>
                	<option value="AZ">Arizona</option>
                	<option value="AR">Arkansas</option>
                	<option value="CA">California</option>
                	<option value="CO">Colorado</option>
                	<option value="CT">Connecticut</option>
                	<option value="DE">Delaware</option>
                	<option value="DC">District Of Columbia</option>
                	<option value="FL">Florida</option>
                	<option value="GA">Georgia</option>
                	<option value="HI">Hawaii</option>
                	<option value="ID">Idaho</option>
                	<option value="IL">Illinois</option>
                	<option value="IN">Indiana</option>
                	<option value="IA">Iowa</option>
                	<option value="KS">Kansas</option>
                	<option value="KY">Kentucky</option>
                	<option value="LA">Louisiana</option>
                	<option value="ME">Maine</option>
                	<option value="MD">Maryland</option>
                	<option value="MA">Massachusetts</option>
                	<option value="MI">Michigan</option>
                	<option value="MN">Minnesota</option>
                	<option value="MS">Mississippi</option>
                	<option value="MO">Missouri</option>
                	<option value="MT">Montana</option>
                	<option value="NE">Nebraska</option>
                	<option value="NV">Nevada</option>
                	<option value="NH">New Hampshire</option>
                	<option value="NJ">New Jersey</option>
                	<option value="NM">New Mexico</option>
                	<option value="NY">New York</option>
                	<option value="NC">North Carolina</option>
                	<option value="ND">North Dakota</option>
                	<option value="OH">Ohio</option>
                	<option value="OK">Oklahoma</option>
                	<option value="OR">Oregon</option>
                	<option value="PA">Pennsylvania</option>
                	<option value="RI">Rhode Island</option>
                	<option value="SC">South Carolina</option>
                	<option value="SD">South Dakota</option>
                	<option value="TN">Tennessee</option>
                	<option value="TX">Texas</option>
                	<option value="UT">Utah</option>
                	<option value="VT">Vermont</option>
                	<option value="VA">Virginia</option>
                	<option value="WA">Washington</option>
                	<option value="WV">West Virginia</option>
                	<option value="WI">Wisconsin</option>
                	<option value="WY">Wyoming</option>
              </select>

              @if ($errors->has('state'))
                  <span class="help-block">
                      <strong>{{ $errors->first('state') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
          <label for="phone" class="col-md-4 control-label">Phone</label>

          <div class="col-md-6">
              <input id="phone" type="phone" class="form-control" name="phone" value="{{ old('phone') }}" required>

              @if ($errors->has('phone'))
                  <span class="help-block">
                      <strong>{{ $errors->first('phone') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group{{ $errors->has('fax') ? ' has-error' : '' }}">
          <label for="fax" class="col-md-4 control-label">Fax</label>

          <div class="col-md-6">
              <input id="fax" type="phone" class="form-control" name="fax" value="{{ old('fax') }}">

              @if ($errors->has('fax'))
                  <span class="help-block">
                      <strong>{{ $errors->first('fax') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group{{ $errors->has('tollFree') ? ' has-error' : '' }}">
          <label for="tollFree" class="col-md-4 control-label">Toll Free</label>

          <div class="col-md-6">
              <input id="tollFree" type="phone" class="form-control" name="tollFree" value="{{ old('tollFree') }}">

              @if ($errors->has('tollFree'))
                  <span class="help-block">
                      <strong>{{ $errors->first('tollFree') }}</strong>
                  </span>
              @endif
          </div>
      </div>
      @if(!isset($type))
      <div class="form-control">
          <button type="button" class="btn yellow cancel">Cancel</button>
          <button type="submit" form="main-form" class="btn green">Submit</button>
      </div>
      @endif
  </form>
</div>
<script>
$(".cancel").off("click").on("click", function(){
  $(".form-container").prev().removeClass("hidden");
  $(".form-container").removeClass("box");
  $(".form-container").html("");
});
</script>
