@extends('layouts.app')

@section('content')
        <div class="box">
            <a href="{{route('warranties.create')}}" class="btn blue create">Create</a>
        </div>
        <div class="form-container"></div>
        <div class="page-header">
            <h2>{{$pageName}}</h2>
        </div>


        <div class="box">
            <div class="table-wrapper">
              <table class="display" style="width:100%">
                <thead>
                  <tr>
                    @foreach($headers as $header)
                      <th>{{$header}}</th>
                    @endforeach
                  </tr>
                </thead>
              </table>
            </div>
        </div>

@endsection
@section('scripts')
<script>
  $("table").DataTable( {
      destroy: true,
      data: {!! json_encode($data) !!},
      "bProcessing": true,
      destroy: true,
      DT_RowId: 'id',
      columns: {!! json_encode($columns) !!}
  });
  $("table").off("click").on("click", "tbody tr .edit", function(){
    var id = $(this).parent().parent().attr("id");
    var path = window.location.pathname;
    window.location.href = path+"/"+id;
});
</script>
@endsection
