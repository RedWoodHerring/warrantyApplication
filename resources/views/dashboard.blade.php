@extends('layouts.app')

@section('content')
    <main class="app-body">
        <div class="page-header">
            <h2>Dashboard</h2>
        </div>
        <div class="dashboard-nav row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="{{ route('projects.create') }}">
                        <i class="fas fa-plus"></i>
                        <span>Create Warranty</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-history"></i>
                        <span>Order History</span>
                    </a>
                </div>
            </div>
            @if(Auth::user()->hasType('A'))
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="{{route('franchises.index')}}">
                        <i class="fas fa-plus"></i>
                        <span>Add Franchise</span>
                    </a>
                </div>
            </div>
            @endif
            @if(Auth::user()->hasType('F'))
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="/freports.php">
                        <i class="fas fa-copy"></i>
                        <span>Franchise Reports</span>
                    </a>
                </div>
            </div>
            @endif
            @if(Auth::user()->hasType('A'))
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-edit"></i>
                        <span>Update or Transfer Warranties</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-print"></i>
                        <span>Print Surveys</span>
                    </a>
                </div>
            </div>
            @endif
            @if(Auth::user()->hasType('A'))
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="/areports.php">
                        <i class="fas fa-copy"></i>
                        <span>Admin Reports</span>
                    </a>
                </div>
            </div>
            @endif
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-user-circle"></i>
                        <span>My Account</span>
                    </a>
                </div>
            </div>
            @if(Auth::user()->hasType('A'))
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="{{route('users.index')}}">
                        <i class="fas fa-users"></i>
                        <span>User Management</span>
                    </a>
                </div>
            </div>
            @endif
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="{{route('customers.listing')}}">
                        <i class="fas fa-users"></i>
                        <span>Customer Management</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="/resources.php">
                        <i class="fas fa-users"></i>
                        <span>Resources</span>
                    </a>
                </div>
            </div>
        </div>
@endsection
