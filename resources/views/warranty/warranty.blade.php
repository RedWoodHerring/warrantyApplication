<div class="warrantyType">
    <label for="search">Select Warranty Type</label>
    <select name="type" id="type">
        <option value="">Please Select A Warranty Type</option>
        <option value="dmGutter">Duck Matte Gutter</option>
        <option value="dmSiding">Duck Matte Siding</option>
        <option value="pvcGutter">PVC Gutter</option>
        <option value="pvcSiding">PVC Siding</option>
        <option value="roofing">Roofing</option>
    </select>
</div>
<script>
$("#type").change(function(){
    var type = $(this).val();
    $.ajax({
    method: "GET",
    url: "/warranties/create/type/"+type
    })
    .done(function(data) {
        $("#warrantyForm").html(data);
        $("#warrantyCreation").removeClass("hidden");
        $("#submit").removeClass("hidden");
        $("#nextButton").addClass("hidden");
    });
});
</script>
