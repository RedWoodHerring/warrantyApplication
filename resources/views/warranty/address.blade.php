<div class="customer">
    <h4>Please Select An Address</h4>
    <ul id="addressListing">
        @foreach($addresses as $address)
        <li class="address" id="{{$address['id']}}">{{$address['address']}} {{$address['city']}} {{$address['state']}} {{$address['zip']}}</li>
        @endforeach
    </ul>
    <button class="hidden" id="newCustomer" type="button" name="button">Create A New Customer</button>
</div>
<script>
$("#addressListing").off("click").on("click",'.address',function(){
    var id = $(this).attr('id');
    $("input[name='address']").val(id);
});
$("#nextButton").off("click").on("click",function(){
    if($("input[name='address']").val() !== ""){
        var id = $("input[name='address']").val();
        var text = $("#"+id).html();
        $.ajax({
        method: "GET",
        url: "/warranties/create/type/"
        })
        .done(function(data) {
            $("#stepBox").html(data);
            $("#showAddress").html(text);
            $("#showAddress").parent().removeClass('hidden')
        });
    }
});
</script>
