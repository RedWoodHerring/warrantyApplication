<div class="customer">
    <label for="search">Search Existing Customers</label>
    <input id="search" type="search" name="search">
    <ul id="searchListing"></ul>
    <button class="hidden" id="newCustomer" type="button" name="button">Create A New Customer</button>
</div>
<script>
    $("#search").keyup(function(){
        $("#searchListing").html("");
        var search = $(this).val();
        if (search.length > 0){
            $.ajax({
            method: "GET",
            url: window.location.pathname + "/"+search
          })
          .done(function(data) {
              var html = "";
              //console.log(data);
              $.each(data, function(index,value){

                  html += "<li class='customer' id='"+value['id']+"'>";
                  $.each(value, function(key,info){

                      if(key === 'name'){
                          html += info+" ";
                      }
                  });
                  html += "</li>";

              });
              $("#searchListing").html(html);
          });
        }
    });
    $("#newCustomer").off("click").on("click", function(){
        $.ajax({
        method: "GET",
        url: "/customers/create"
      })
      .done(function(data) {
        $("#newCustomer").parent().html(data);
      });
    });
    $("#searchListing").off("click").on("click",'.customer',function(){
        var id = $(this).attr('id');
        $("input[name='customer']").val(id);
    });
    $("#nextButton").off("click").on("click",function(){
        if($("input[name='customer']").val() !== ""){
            var id = $("input[name='customer']").val();
            var text = $("#"+id).html();
            $.ajax({
            method: "GET",
            url: "/warranties/create/addresses/"+id
            })
            .done(function(data) {
                $("#stepBox").html(data);
                $("#showCustomer").html(text);
                $("#showCustomer").parent().removeClass('hidden')
            });
        }
    });
</script>
