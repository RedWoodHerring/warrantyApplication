@extends('layouts.details')


@section('subContent')
    <div class="details">
        <div class="info-group">
            <h4>Customer</h4>
            <p>{{$data['customer']}}</p>
        </div>
        <div class="info-group">
            <h4>Type</h4>
            <p>{{$data['type']}}</p>
        </div>
        <div class="info-group">
            <h4>Date</h4>
            <p>{{$data['date']}}</p>
        </div>
        <form id="deleteForm" class="form-horizontal" method="POST">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="DELETE">
            <button type="submit" form="deleteForm" class="btn red">Delete</button>
        </form>
    </div>
@endsection


@section('contentScripts')
    <script>
      $(".edit").off("click").on("click", function(){
        $(this).parent().next("div").toggleClass("collapsed");
        $(this).parent().next("div").toggleClass("opened");
      });
    </script>
@endsection
