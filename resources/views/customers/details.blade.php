@extends('layouts.details')


@section('subContent')
    <div class="details">
        <div class="info-group">
            <h4>Customer Name</h4>
            <p>{{$data['name']}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="name-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">

                <input id="name" type="text" class="form-control" name="name" value="{{ $data['name'] }}" required autofocus>

                <button type="submit" form="name-form" class="btn green update-btn">Change</button>
            </form>
        </div>
    </div>
    <h3>Primary Contact</h3>
    <div class="details">
        <div class="info-group">
            <h4>Name</h4>
            <p>{{$data['fName']}} {{$data['lName']}}</p>
        </div>

        <div class="info-group">
            <h4>Email</h4>
            <p>{{$data['email']}}</p>
        </div>

        <div class="info-group">
            <h4>Phone</h4>
            <p>{{$data['phone']}}</p>
        </div>
    </div>
    <h3>Primary Address</h3>
    <div class="details">
        <div class="info-group">
            <h4>Address</h4>
            <p>{{$data['address']}}</p>
        </div>

        <div class="info-group">
            <h4>City</h4>
            <p>{{$data['city']}}</p>
        </div>

        <div class="info-group">
            <h4>State</h4>
            <p>{{$data['state']}}</p>
        </div>

        <div class="info-group">
            <h4>ZipCode</h4>
            <p>{{$data['zip']}}</p>
        </div>

        <div class="info-group">
            <h4>County</h4>
            <p>{{$data['county']}}</p>
        </div>




    </div>
    <form id="deleteForm" class="form-horizontal" method="POST">
        {{ csrf_field() }}
        <input name="_method" type="hidden" value="DELETE">
        <button type="submit" form="deleteForm" class="btn red">Delete</button>
    </form>
@endsection


@section('contentScripts')
    <script>
      $(".edit").off("click").on("click", function(){
        $(this).parent().next("div").toggleClass("collapsed");
        $(this).parent().next("div").toggleClass("opened");
      });
    </script>
@endsection
