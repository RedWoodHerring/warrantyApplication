@extends('layouts.details')

@section('subContent')
<div class="details">

@foreach($data as $key => $address)
@if($address['type'] === 'M')
<p>Master Address</p>
@endif
    <div class="info-group">
        <h4>{{$address['address']}}</h4>
        <h4>{{$address['city']}}</h4>
        <h4>{{$address['state']}}</h4>
        <h4>{{$address['zip']}}</h4>
        <h4>{{$address['county']}}</h4>
        <button class="btn yellow edit"><i class="fas fa-edit"></i></button>

        @if($address['type'] !== 'M')
        <button type="submit" form="{{$address['id']}}-master" id="master" class="btn blue">M</button>
        <button type="submit" form="{{$address['id']}}-delete" class="btn red delete"><i class="fas fa-minus-square"></i></button>
        @endif

        <form id="{{$address['id']}}-delete" class="form-horizontal hidden" method="POST" action="{{route('customers.addresses.destroy', ['key' => $address['id']])}}">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="DELETE">
            <input type="hidden" name="key" value="{{$address['id']}}">
        </form>

        <form id="{{$address['id']}}-master" class="form-horizontal hidden" method="POST" action="{{route('customers.address.master', ['key' => $address['id']])}}">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            <input type="hidden" name="key" value="{{$address['id']}}">
        </form>
    </div>

    <div class="update-form form-wrapper collapsed">
        <form id="{{$key}}-form" class="form-horizontal" method="POST">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">

            <input type="hidden" name="key" value="{{$address['id']}}">

            <input id="address" type="text" class="form-control" name="address" value="{{$address['address']}}" required autofocus>

            <input id="city" type="text" class="form-control" name="city" value="{{$address['city']}}" required autofocus>

            <select id="state" class="form-control" name="state" required autofocus>
                @foreach($states as $val => $state)
                    @if($val === $address['state'])
                        <option value="{{$val}}" selected>{{$state}}</option>
                    @else
                        <option value="{{$val}}">{{$state}}</option>
                    @endif
                @endforeach
            </select>

            <input id="zip" type="number" class="form-control" name="zip" value="{{$address['zip']}}" required autofocus>

            <input id="county" type="text" class="form-control" name="county" value="{{$address['county']}}" required autofocus>

            <button type="submit" form="{{$key}}-form" class="btn green update-btn">Change</button>
        </form>
    </div>
@endforeach

<button id="addAddress" class="btn green">Add Address</button>

</div>
@endsection

@section('contentScripts')
    <script>
        $("#infoType").val('address');

        $(".edit").off("click").on("click", function(){
          $(this).parent().next("div").toggleClass("collapsed");
          $(this).parent().next("div").toggleClass("opened");
        });

        $("#addAddress").off("click").on("click", function(){
            $.ajax({
            method: "GET",
            url: window.location.pathname + "/create"
          })
          .done(function(data) {
            $(".details").html(data)
          });
        });

    </script>
@endsection
