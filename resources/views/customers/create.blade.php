
  @if(isset($redirect))
      <input id="redirect" type="text" class="hidden" name="redirect" value="yes" required>
  @endif
  @if(Auth::user()->hasType('A'))
  <div class="form-group{{ $errors->has('franchise') ? ' has-error' : '' }}">
      <label for="franchise" class="col-md-4 control-label">Franchise</label>

      <div class="col-md-6">
          <select id="franchise" class="form-control" name="franchise" required autofocus>
              @foreach($franchises as $franchise)
                <option value="{{$franchise['id']}}">{{$franchise['name']}}</option>
              @endforeach
          </select>

          @if ($errors->has('franchise'))
              <span class="help-block">
                  <strong>{{ $errors->first('franchise') }}</strong>
              </span>
          @endif
      </div>
  </div>
  @elseif(Auth::user()->hasType('F'))
  <div class="hidden form-group{{ $errors->has('franchise') ? ' has-error' : '' }}">
      <label for="franchise" class="col-md-4 control-label">Franchise</label>

      <div class="col-md-6">
          <select id="franchise" class="form-control" name="franchise" required autofocus>
                <option value="{{Auth::user()->getFranchise()}}"></option>
          </select>
      </div>
  </div>
  @endif
  <div class="form-group{{ $errors->has('fName') ? ' has-error' : '' }}">
      <label for="nname" class="col-md-4 control-label">Customer Name</label>

      <div class="col-md-6">
          <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

          @if ($errors->has('name'))
              <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
              </span>
          @endif
      </div>
  </div>
  <hr>
  <h4>Primary Contact</h4>
  <div class="form-group{{ $errors->has('fName') ? ' has-error' : '' }}">
      <label for="fName" class="col-md-4 control-label">First Name</label>

      <div class="col-md-6">
          <input id="fName" type="text" class="form-control" name="fName" value="{{ old('fName') }}" required autofocus>

          @if ($errors->has('fName'))
              <span class="help-block">
                  <strong>{{ $errors->first('fName') }}</strong>
              </span>
          @endif
      </div>
  </div>

  <div class="form-group{{ $errors->has('lName') ? ' has-error' : '' }}">
      <label for="lName" class="col-md-4 control-label">Last Name</label>

      <div class="col-md-6">
          <input id="lName" type="text" class="form-control" name="lName" value="{{ old('lName') }}" required autofocus>

          @if ($errors->has('lName'))
              <span class="help-block">
                  <strong>{{ $errors->first('lName') }}</strong>
              </span>
          @endif
      </div>
  </div>

  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      <label for="email" class="col-md-4 control-label">Email</label>

      <div class="col-md-6">
          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

          @if ($errors->has('email'))
              <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
      </div>
  </div>

  <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
      <label for="phone" class="col-md-4 control-label">Phone</label>

      <div class="col-md-6">
          <input id="phone" type="phone" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

          @if ($errors->has('phone'))
              <span class="help-block">
                  <strong>{{ $errors->first('phone') }}</strong>
              </span>
          @endif
      </div>
  </div>

  <hr>
  <h4>Primary Location</h4>
  <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
      <label for="address" class="col-md-4 control-label">Address</label>

      <div class="col-md-6">
          <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus>

          @if ($errors->has('address'))
              <span class="help-block">
                  <strong>{{ $errors->first('address') }}</strong>
              </span>
          @endif
      </div>
  </div>

  <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
      <label for="city" class="col-md-4 control-label">City</label>

      <div class="col-md-6">
          <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" required autofocus>

          @if ($errors->has('city'))
              <span class="help-block">
                  <strong>{{ $errors->first('city') }}</strong>
              </span>
          @endif
      </div>
  </div>

  <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
      <label for="state" class="col-md-4 control-label">State</label>

      <div class="col-md-6">
          <select id="state" class="form-control" name="state" required autofocus>
              <option value="AL">Alabama</option>
              <option value="AK">Alaska</option>
              <option value="AZ">Arizona</option>
              <option value="AR">Arkansas</option>
              <option value="CA">California</option>
              <option value="CO">Colorado</option>
              <option value="CT">Connecticut</option>
              <option value="DE">Delaware</option>
              <option value="DC">District Of Columbia</option>
              <option value="FL">Florida</option>
              <option value="GA">Georgia</option>
              <option value="HI">Hawaii</option>
              <option value="ID">Idaho</option>
              <option value="IL">Illinois</option>
              <option value="IN">Indiana</option>
              <option value="IA">Iowa</option>
              <option value="KS">Kansas</option>
              <option value="KY">Kentucky</option>
              <option value="LA">Louisiana</option>
              <option value="ME">Maine</option>
              <option value="MD">Maryland</option>
              <option value="MA">Massachusetts</option>
              <option value="MI">Michigan</option>
              <option value="MN">Minnesota</option>
              <option value="MS">Mississippi</option>
              <option value="MO">Missouri</option>
              <option value="MT">Montana</option>
              <option value="NE">Nebraska</option>
              <option value="NV">Nevada</option>
              <option value="NH">New Hampshire</option>
              <option value="NJ">New Jersey</option>
              <option value="NM">New Mexico</option>
              <option value="NY">New York</option>
              <option value="NC">North Carolina</option>
              <option value="ND">North Dakota</option>
              <option value="OH">Ohio</option>
              <option value="OK">Oklahoma</option>
              <option value="OR">Oregon</option>
              <option value="PA">Pennsylvania</option>
              <option value="RI">Rhode Island</option>
              <option value="SC">South Carolina</option>
              <option value="SD">South Dakota</option>
              <option value="TN">Tennessee</option>
              <option value="TX">Texas</option>
              <option value="UT">Utah</option>
              <option value="VT">Vermont</option>
              <option value="VA">Virginia</option>
              <option value="WA">Washington</option>
              <option value="WV">West Virginia</option>
              <option value="WI">Wisconsin</option>
              <option value="WY">Wyoming</option>
          </select>

          @if ($errors->has('state'))
              <span class="help-block">
                  <strong>{{ $errors->first('state') }}</strong>
              </span>
          @endif
      </div>
  </div>

  <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
      <label for="zip" class="col-md-4 control-label">Zip</label>

      <div class="col-md-6">
          <input id="zip" type="text" class="form-control" name="zip" value="{{ old('zip') }}" required autofocus>

          @if ($errors->has('zip'))
              <span class="help-block">
                  <strong>{{ $errors->first('zip') }}</strong>
              </span>
          @endif
      </div>
  </div>

  <div class="form-group{{ $errors->has('county') ? ' has-error' : '' }}">
      <label for="county" class="col-md-4 control-label">County</label>

      <div class="col-md-6">
          <input id="county" type="text" class="form-control" name="county" value="{{ old('county') }}" required autofocus>

          @if ($errors->has('county'))
              <span class="help-block">
                  <strong>{{ $errors->first('county') }}</strong>
              </span>
          @endif
      </div>
  </div>

  <div class="form-group">
      <label for="county" class="col-md-4 control-label">Is this the Billing Address</label>

      <div class="col-md-6">
          <select id="billing" class="form-control" name="billing" required autofocus>
              <option value="yes">Yes</option>
              <option value="no">No</option>
          </select>
      </div>
  </div>
