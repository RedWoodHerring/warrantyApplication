@extends('layouts.details')

@section('subContent')
<div class="details">

@foreach($contacts as $key => $contact)
@if($contact['type'] === 'Primary')
<p>Master Contact</p>
@endif
    <div class="info-group">
        <h4>{{$contact['fName']}}</h4>
        <h4>{{$contact['lName']}}</h4>
        <h4>{{$contact['email']}}</h4>
        <h4>{{$contact['phone']}}</h4>
        <button class="btn yellow edit"><i class="fas fa-edit"></i></button>

        @if($contact['type'] !== 'Primary')
        <button type="submit" form="{{$key}}-master" id="master" class="btn blue">M</button>
        <button type="submit" form="{{$key}}-delete" class="btn red delete"><i class="fas fa-minus-square"></i></button>
        @endif

        <form id="{{$key}}-delete" class="form-horizontal hidden" method="POST" action="{{route('customers.contacts.destroy', ['id' => $id,'key'=> $key])}}">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="DELETE">
            <input type="hidden" name="key" value="{{$key}}">
        </form>

        <form id="{{$key}}-master" class="form-horizontal hidden" method="POST" action="{{$baseHref.'/contacts/master/'.$key}}">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            <input type="hidden" name="key" value="{{$key}}">
        </form>


    </div>
    <div class="update-form form-wrapper collapsed">
        <form id="{{$key}}-form" class="form-horizontal" method="POST">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">

            <input type="hidden" name="key" value="{{$key}}">

            <input id="fName" type="text" class="form-control" name="fName" value="{{$contact['fName']}}" required autofocus>

            <input id="lName" type="text" class="form-control" name="lName" value="{{$contact['lName']}}" required autofocus>

            <input id="email" type="email" class="form-control" name="email" value="{{$contact['email']}}" required autofocus>

            <input id="phone" type="number" class="form-control" name="phone" value="{{$contact['phone']}}" required autofocus>

            <button type="submit" form="{{$key}}-form" class="btn green update-btn">Change</button>
        </form>
    </div>
@endforeach

<button id="addAddress" class="btn green">Add Contact</button>

</div>
@endsection

@section('contentScripts')
    <script>
        $("#infoType").val('contacts');

        $(".edit").off("click").on("click", function(){
          $(this).parent().next("div").toggleClass("collapsed");
          $(this).parent().next("div").toggleClass("opened");
        });

        $("#addAddress").off("click").on("click", function(){
            $.ajax({
            method: "GET",
            url: window.location.pathname + "/create"
          })
          .done(function(data) {
            $(".details").html(data)
          });
        });

    </script>
@endsection
