@extends('layouts.app')

@section('content')
        <div class="page-header">
            <h2>{{$pageName}}</h2>
        </div>
        <div class="box">
            <select class="box-header" id="infoType">
                @if($type === 'customer')
                    <option value="info">Info Display</option>
                    <option value="address">Addresses</option>
                    <option value="warranty">Warranties</option>
                    <option value="contacts">Contacts</option>
                @elseif($type === 'user')
                    <option value="info">Info Display</option>

                @elseif($type === 'franchise')
                    <option value="info">Info Display</option>
                @elseif($type === 'warranty')
                    <option value="info">Info Display</option>
                @endif
            </select>
            <div id="box-container">
                @yield('subContent')
            </div>
        </div>

@endsection
@section('scripts')

@yield('contentScripts')

@if($type === 'customer')
<script>
    $("#infoType").change(function(){
        var type = $(this).val();
        if(type === 'address'){
            window.location.href = "{{$baseHref}}/addresses";
        }
        if(type === 'info'){
            window.location.href = "{{$baseHref}}";
        }
        if(type === 'warranty'){
            window.location.href = "{{$baseHref}}/warranties";
        }

        if(type === 'contacts'){
            window.location.href = "{{$baseHref}}/contacts";
        }
    });
</script>
@endif

@endsection
