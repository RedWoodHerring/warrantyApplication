<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ABC Warranty v0.27b</title>
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/flexboxgrid.min.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
</head>
<body>

    <div id="app">

      @auth
        <div class="nav-panel">
          <nav>
              <ul>
                  <li><a href="/"><i class="fas fa-th-large"></i> Dashboard</a></li>
                  <li class="active"><a href="#"><i class="fas fa-history"></i> Order History</a></li>
                  <li><a href="#"><i class="fas fa-edit"></i> Update or Transfer Warranties</a></li>
                  <li><a href="#"><i class="fas fa-print"></i> Print Surveys</a></li>
                  @if(Auth::user()->hasType('A'))
                    <li><a href="#"><i class="fas fa-copy"></i> Admin Reports</a></li>
                  @endif
                  <li><a href="#"><i class="fas fa-user-circle"></i>My Account</a></li>
                  @if(Auth::user()->hasType('A'))
                    <li><a href="{{ route('users.index') }}"><i class="fas fa-users"></i> User management</a></li>
                  @endif
                  <li><a href="{{ route('customers.listing') }}"><i class="fas fa-users"></i> Customer management</a></li>
              </ul>
          </nav>
        </div>
      <div class="app-content">
        <header class="header">
            <div class="mainnav">
                <div class="navbar">
                    <a href="javascript:void(0)" class="nav-toggle hamburger">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </a>
                    <h2>ABC WARRANTY</h2>
                </div>
                <div class="user-info">
                    <a href="javascript:void(0)" class=""><span><i class="fas fa-user-circle"></i></span><span class="user-name">{{auth()->user()->username}}</span></a>
                    <div class="user-slidein">
                        <ul>
                            <li><a href="#"><i class="fas fa-user"></i> My Account</a></li>
                            <li class="logout">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <main class="app-body">


      @endauth
      @if(!empty($errors->first()))
          <div class="row col-lg-12">
              <div class="alert alert-danger">
                  <p>{{ $errors->first() }}</p>
              </div>
          </div>
      @endif
        @yield('content')
      @auth
          </main>
          <footer class="footer">
              <h5>&copy; ABC SEAMLESS v0.27b</h5>
          </footer>
        </div>
      @endauth
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    @yield('scripts')
</body>
</html>
