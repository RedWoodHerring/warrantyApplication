@extends('layouts.app')

@section('content')
<section class="login-container">
    <div class="right-col">
        <div class="branding">
            <img src="{{ asset('images/abc-logo.png') }}" alt="ABC Seamless">
            <h1>Warranty Center</h1>
        </div>
    </div>
    <div class="left-col">
        <div class="login-wrapper">
            <h2>Login</h2>

                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-row{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">User Name</label>

                                <input id="username" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-row{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-controls">
                                <button type="submit" class="submit-btn">
                                    Submit
                                </button>
                        </div>
                    </form>
</div>
        <div class="copyright">
            <p>&copy; 1978-{{ date('Y')  }} ABC Seamless, Inc. | 3001 Fiechtner Dr S, Fargo, ND 58103 | Please report any incorrect link information to: <a href="mailto:jess@abcseamless.com">jess@abcseamless.com</a></p>
            <h5>Built by <a href="http://results-unlimited.com/">results unlimited</a></h5>
        </div>
    </div>
</section>

@endsection
