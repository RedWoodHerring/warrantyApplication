@extends('layouts.app')

@section('content')
        <div class="page-header">
            <h2>Page Name</h2>
        </div>
        <div class="box">
            <h3 class="box-header">Text & Buttons</h3>
            <h4>Donec sed congue arcu</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus scelerisque in nunc sodales egestas. Fusce sit amet lacinia enim. Donec ullamcorper semper consectetur. Ut porttitor justo eu convallis commodo. Aliquam vel vehicula orci. In eu magna fermentum magna mattis blandit. Integer lobortis in lorem a suscipit. In eu magna fermentum magna mattis blandit.</p>
            <hr>
            <div class="btn-container">
                <button class="btn yellow">Button Text</button>
                <button class="btn green">Button Text</button>
                <button class="btn blue">Button Text</button>
                <button class="btn red">Button Text</button>
                <button class="btn grey">Button Text</button>
            </div>
        </div>
        <div class="box">
            <h3 class="box-header">Table Filter</h3>
            <div class="table-wrapper">
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Order #</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zipcode</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="box">
            <h3 class="box-header">Basic Form</h3>
            <div class="form-wrapper">
                <form action="" method="POST">
                    <div class="row">
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="" >Text Input</label>
                            <input type="text">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="">Text Input</label>
                            <input type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="">Select</label>
                                <select class="" name="">
                                    <option value="" disabled="disabled" selected="selected">Select ...</option>
                                    <option value="">Option Value</option>
                                    <option value="">Option Value</option>
                                    <option value="">Option Value</option>
                                    <option value="">Option Value</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group checkbox-wrapper">
                                <div class="checkbox-group">
                                    <input type="checkbox">
                                    <label for="">Lorem Ipsum Dolor</label>
                                </div>
                                <div class="checkbox-group">
                                    <input type="checkbox">
                                    <label for="">Lorem Ipsum</label>
                                </div>
                                <div class="checkbox-group">
                                    <input type="checkbox">
                                    <label for="">Yes</label>
                                </div>
                                <div class="checkbox-group">
                                    <input type="checkbox">
                                    <label for="">No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="">Text Area</label>
                                <textarea name="name" placeholder="Notes..."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-controls">
                        <button type="submit" class="btn yellow">Submit</button>
                        <button type="reset" class="btn grey">Reset</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="box">
            <h3 class="box-header">Info Display</h3>
            <div class="details">
                <div class="info-group">
                    <h4>Donec sed congue arcu</h4>
                    <p>Lorem ipsum dolor</p>
                    <button><i class="fas fa-edit"></i></button>
                </div>
                <div class="info-group">
                    <h4>Donec et blandit</h4>
                    <p>Laculis nunc</p>
                    <button><i class="fas fa-edit"></i></button>
                </div>
                <div class="info-group">
                    <h4>Vitae tellus</h4>
                    <p>Justo Porttitor</p>
                    <button><i class="fas fa-edit"></i></button>
                </div>
                <div class="info-group">
                    <h4>Aliquam</h4>
                    <p>Lorem ipsum</p>
                    <button><i class="fas fa-edit"></i></button>
                </div>
            </div>
        </div>

@endsection
