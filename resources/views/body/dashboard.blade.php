@extends('layouts.app')

@section('content')
<div class="nav-panel">
    <nav>
        <ul>
            <li><a href="/"><i class="fas fa-th-large"></i> Dashboard</a></li>
            <li class="active"><a href="#"><i class="fas fa-history"></i> Order History</a></li>
            <li><a href="#"><i class="fas fa-edit"></i> Update or Transfer Warranties</a></li>
            <li><a href="#"><i class="fas fa-print"></i> Print Surveys</a></li>
            <li class="has-submenu">
                <a href="#"><i class="fas fa-copy"></i> Admin Reports</a>
                <span class="subnav-toggle">></span>
                <ul>
                    <li><a href="#">Submenu Item</a></li>
                    <li><a href="#">Submenu Item</a></li>
                    <li><a href="#">Submenu Item</a></li>
                    <li><a href="#">Submenu Item</a></li>
                </ul>
            </li>
            <li><a href="#"><i class="fas fa-user-circle"></i> My Account</a></li>
            <li><a href="#"><i class="fas fa-users"></i> User management</a></li>
        </ul>
    </nav>
</div>
<div class="app-content">
    <header class="header">
        <div class="mainnav">
            <div class="navbar">
                <a href="javascript:void(0)" class="nav-toggle hamburger">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </a>
                <h2>ABC WARRANTY</h2>
            </div>
            <div class="user-info">
                <a href="javascript:void(0)" class=""><span><i class="fas fa-user-circle"></i></span> User Name</a>
                <div class="user-slidein">
                    <ul>
                        <li><a href="#"><i class="fas fa-user"></i> My Account</a></li>
                        <li><a href="#"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <main class="app-body">
        <div class="page-header">
            <h2>Dashboard</h2>
        </div>
        <div class="dashboard-nav row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-plus"></i>
                        <span>Create Warranty</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-history"></i>
                        <span>Order History</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-plus"></i>
                        <span>Add Franchise</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-edit"></i>
                        <span>Update or Transfer Warranties</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-print"></i>
                        <span>Print Surveys</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-copy"></i>
                        <span>Admin Reports</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-user-circle"></i>
                        <span>My Account</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box">
                    <a href="#">
                        <i class="fas fa-users"></i>
                        <span>User Management</span>
                    </a>
                </div>
            </div>
        </div>
    </main>
    <footer class="footer">
        <h5>&copy; ABC SEAMLESS</h5>
    </footer>
</div>
@endsection
