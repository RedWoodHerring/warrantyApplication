@extends('layouts.details')


@section('subContent')
    <div class="details">
    @foreach($data as $key => $detail)
        @if($key === "franchise" && Auth::user()->hasType('F'))

        @else
        <div class="info-group">
            <h4>{{$key}}</h4>
            <p>{{$detail}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="{{$key}}-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">
                @if($key === "type")
                <select id="{{$key}}" class="form-control" name="{{$key}}" required autofocus>
                    <option value=''>Please Select A Type</option>
                    <option value='C'>Customer</option>
                    <option value='F'>Franchise</option>
                    <option value='A'>Admin</option>
                </select>
                @else
                <input id="{{$key}}" type="text" class="form-control" name="{{$key}}" value="{{ $detail }}" required autofocus>
                @endif

                <button type="submit" form="{{$key}}-form" class="btn green update-btn">Change</button>
            </form>
        </div>
        @endif

    @endforeach

        <form id="deleteForm" class="form-horizontal" method="POST">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="DELETE">
            <button type="submit" form="deleteForm" class="btn red">Delete</button>
        </form>

    </div>
@endsection


@section('contentScripts')
    <script>
      $(".edit").off("click").on("click", function(){
        $(this).parent().next("div").toggleClass("collapsed");
        $(this).parent().next("div").toggleClass("opened");
      });
    </script>
@endsection
