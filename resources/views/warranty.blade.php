@extends('layouts.app')

@section('content')
<div class="page-header">
    <h2>Create A Warranty</h2>
</div>
<div class="box">

    <section id="stepBox">
        <button id="searchCustomer" class="btn yellow">Search Existing Customer</button>
        <button id="createCustomer" class="btn yellow">Create A New Customer</button>
    </section>
    <hr>
    <h4 class="hidden">Customer: <span id="showCustomer"></span></h4>
    <h4 class="hidden">Address: <span id="showAddress"></span></h4>
    <section class="form-wrapper">
        <form id="warrantyCreation" class="hidden" method="POST" action="{{route('warranties.store')}}">
            {{ csrf_field() }}
            @if(isset($id))
            <input type="text" class="form-control hidden" name="customer" value="{{$id}}">
            @else
            <input type="text" class="form-control hidden" name="customer">
            @endif

            @if(isset($id))
            <input type="text" class="form-control hidden" name="address" value="{{$address}}">
            @else
            <input type="text" class="form-control hidden" name="address">
            @endif
            <div class="form-group{{ $errors->has('consultant') ? ' has-error' : '' }}">
              <label for="consultant" class="col-md-4 control-label">Sales Consultant</label>

              <div class="col-md-6">
                  <input id="consultant" type="text" class="form-control" name="consultant" value="{{ old('consultant') }}" required autofocus>

              </div>
            </div>

            <div class="form-group{{ $errors->has('Installer') ? ' has-error' : '' }}">
              <label for="Installer" class="col-md-4 control-label">Installer's Name</label>

              <div class="col-md-6">
                  <input id="Installer" type="text" class="form-control" name="Installer" value="{{ old('Installer') }}" required autofocus>

              </div>
            </div>

            <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
              <label for="date" class="col-md-4 control-label">Installation Date</label>

              <div class="col-md-6">
                  <input id="date" type="date" class="form-control" name="date" value="{{ old('date') }}" required autofocus>

              </div>
            </div>
            <div id="warrantyForm"></div>
        </form>
    </section>
    <section id="form-navigation">
        <button id="backButton" class="btn blue hidden">Back</button>
        <button id="nextButton" class="btn blue hidden">Next</button>
        <button id="submit" type="submit" form="warrantyCreation" class="btn blue hidden">Next</button>
        <button id="resetButton" class="btn yellow hidden">Reset</button>
    </section>
</div>


@endsection
@section('scripts')
    <script>
        $("#searchCustomer").off("click").on("click", function(){
            $.ajax({
            method: "GET",
            url: "/warranties/create/customers"
          })
          .done(function(data) {
              $("#stepBox").html(data);
              $("#nextButton").removeClass("hidden");
              $("#resetButton").removeClass("hidden");
          });
        });


        $("#resetButton").off("click").on("click",function(){
            location.reload();
        });
    </script>
    <script>
        $("#createCustomer").off("click").on("click", function(){
            $.ajax({
                method: "GET",
                url: "/customers/create/yes"
            })
            .done(function(data) {
                $("#stepBox").html(data);
            });
        });
    </script>
@endsection
