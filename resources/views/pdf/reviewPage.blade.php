@extends('layouts.projectPDF')

@section('content')
<iframe src="{{'public/page1.pdf'}}" style="width: 100vw; height: 100vh;position:absolute;top:0;z-index:0;"></iframe>
<div style="position:fixed;width:100%;height:100%;z-index:1"></div>
<div id="content" style="position: absolute;top:50%;left: calc(50% - 4cm);z-index:2">
<h1 style="font-size:20px;background:#00ff00;">I am on top of the PDF document!</h1>
</div>
@endsection
