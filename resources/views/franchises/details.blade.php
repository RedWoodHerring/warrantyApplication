@extends('layouts.details')


@section('subContent')
    <div class="details">
        <div class="info-group">
            <h4>Name</h4>
            <p>{{$data['name']}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="name-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">

                <input id="name" type="text" class="form-control" name="name" value="{{ $data['name'] }}" required autofocus>

                <button type="submit" form="name-form" class="btn green update-btn">Change</button>
            </form>
        </div>

        <div class="info-group">
            <h4>Username</h4>
            <p>{{$data['username']}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="username-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">

                <input id="username" type="text" class="form-control" name="username" value="{{ $data['username'] }}" required autofocus>

                <button type="submit" form="username-form" class="btn green update-btn">Change</button>
            </form>
        </div>

        <div class="info-group">
            <h4>Address</h4>
            <p>{{$data['address']}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="address-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">

                <input id="address" type="text" class="form-control" name="address" value="{{ $data['address'] }}" required autofocus>

                <button type="submit" form="address-form" class="btn green update-btn">Change</button>
            </form>
        </div>

        <div class="info-group">
            <h4>City</h4>
            <p>{{$data['city']}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="city-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">

                <input id="city" type="text" class="form-control" name="city" value="{{ $data['city'] }}" required autofocus>

                <button type="submit" form="city-form" class="btn green update-btn">Change</button>
            </form>
        </div>

        <div class="info-group">
            <h4>State</h4>
            <p>{{$data['state']}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="state-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">

                <input id="state" type="text" class="form-control" name="state" value="{{ $data['state'] }}" required autofocus>

                <button type="submit" form="state-form" class="btn green update-btn">Change</button>
            </form>
        </div>

        <div class="info-group">
            <h4>ZipCode</h4>
            <p>{{$data['zip']}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="zip-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">

                <input id="zip" type="text" class="form-control" name="zip" value="{{ $data['zip'] }}" required autofocus>

                <button type="submit" form="zip-form" class="btn green update-btn">Change</button>
            </form>
        </div>

        <div class="info-group">
            <h4>Phone Number</h4>
            <p>{{$data['phone']}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="phone-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">

                <input id="phone" type="text" class="form-control" name="phone" value="{{ $data['phone'] }}" required autofocus>

                <button type="submit" form="phone-form" class="btn green update-btn">Change</button>
            </form>
        </div>

        <div class="info-group">
            <h4>Toll Free Number</h4>
            <p>{{$data['tollFree']}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="tollFree-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">

                <input id="tollFree" type="text" class="form-control" name="tollFree" value="{{ $data['tollFree'] }}" required autofocus>

                <button type="submit" form="tollFree-form" class="btn green update-btn">Change</button>
            </form>
        </div>

        <div class="info-group">
            <h4>Fax Number</h4>
            <p>{{$data['fax']}}</p>
            <button class="edit"><i class="fas fa-edit"></i></button>
        </div>
        <div class="update-form form-wrapper collapsed">
            <form id="fax-form" class="form-horizontal" method="POST">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">

                <input id="fax" type="text" class="form-control" name="fax" value="{{ $data['fax'] }}" required autofocus>

                <button type="submit" form="fax-form" class="btn green update-btn">Change</button>
            </form>
        </div>

        <form id="deleteForm" class="form-horizontal" method="POST">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="DELETE">
            <button type="submit" form="deleteForm" class="btn red">Delete</button>
        </form>

    </div>
@endsection


@section('contentScripts')
    <script>
      $(".edit").off("click").on("click", function(){
        $(this).parent().next("div").toggleClass("collapsed");
        $(this).parent().next("div").toggleClass("opened");
      });
    </script>
@endsection
