@extends('layouts.app')

@section('content')
<div class="page-header">
    <h2>Create A Project</h2>
</div>
<div class="box">
    <section id="formContent">
      @yield('formContent')
    </section>
    <section id="form-navigation">
        <button id="backButton" class="btn blue hidden">Back</button>
        <button id="nextButton" class="btn blue hidden">Next</button>
        <button id="submit" type="submit" form="warrantyCreation" class="btn blue hidden">Next</button>
        <button id="resetButton" class="btn yellow hidden">Reset</button>
    </section>
</div>


@endsection
@section('scripts')
    <script>
    </script>
@endsection
