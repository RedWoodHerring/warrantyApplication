@extends('projects.create')

@section('formContent')
<h2>Customer</h2>
<div class="table-wrapper">
    <table>
        <thead>
            <tr>
                <th>Customer Name</th>
            </tr>
        </thead>
    </table>
</div>
<div class="form-wrapper hidden">
    <form></form>
</div>
<button class="btn yellow" id="newCustomer">Create A New Customer</button>
<button class="btn yellow hidden" id="existingCustomer">Use Existing Customer</button>
<input type="hidden" id="customerID"/>


@endsection
@section('scripts')
    <script>
        // Pull Listing of Customers
        $.get( "{{route('customers.index')}}", function( data ) {
            $("table").DataTable( {
                destroy: true,
                data: data,
                "bProcessing": true,
                destroy: true,
                DT_RowId: 'id',
                columns: [
                    { "data": "name" }
                ]
            });
        });

        //Add Customer ID to input
        $("table").off("click").on("click", 'tbody tr', function(){
            customerID = $(this).attr('id');

            $("table").children("tbody").children("tr").each(function(){
                if($(this).hasClass("selected")){
                    $(this).removeClass("selected");
                } else {
                    $(this).addClass("selected");
                }
            });

            if(customerID !== ""){
                $(this).addClass("selected");
                $("#customerID").val(customerID);
                $("#nextButton").removeClass("hidden");
            }

        });

        //Create New Customer Button click
        $("#newCustomer").off("click").on("click",function(){
            $(this).addClass("hidden");
            $("#existingCustomer").removeClass("hidden");
            $.get( "{{route('customers.create')}}", function( data ) {
                $("form").html(data);
                $(".form-wrapper").removeClass("hidden");
                $(".table-wrapper").addClass("hidden");
                $("#nextButton").removeClass("hidden");
            });
        });

        //Return to Existing Customers
        $("#existingCustomer").off("click").on("click",function(){
            $(this).addClass("hidden");
            $("#newCustomer").removeClass("hidden");
            $(".table-wrapper").removeClass("hidden");
            $("form").html("");
            $(".form-wrapper").addClass("hidden");
            $("#nextButton").addClass("hidden");
            $.get( "{{route('customers.index')}}", function( data ) {
                $("table").DataTable( {
                    destroy: true,
                    data: data,
                    "bProcessing": true,
                    destroy: true,
                    DT_RowId: 'id',
                    columns: [
                        { "data": "name" }
                    ]
                });
            });
        });

        //Next Button click
        $("#nextButton").off("click").on("click", function(){
            if($(".table-wrapper").hasClass("hidden")){
                data = $("form").serialize()+"&type=newCustomer";
                if($("select[name='billing']").val() === "yes"){
                    url = "/projects/create/address/"
                } else {
                    url = "/projects/create/0/billing/"
                }

            }

            if($(".form-wrapper").hasClass("hidden")){
                data = "id="+$("#customerID").val()+"&type=existingCustomer";

                    url = "/projects/create/"+$("#customerID").val()+"/address"

            }
            $.ajax ({
                url: "{{route('sessionStorage')}}",
                data: data+"&_token={{ csrf_token() }}",
                type: "POST",
                success: function(){
                    window.location.href = url;
                }
            })
        });
    </script>
@endsection
