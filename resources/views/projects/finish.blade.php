@extends('projects.create')

@section('formContent')
    <h3>Thank You For Your Submission</h3>
    <button id="again" class="btn green">Create Another Project</button>
    <button id="dashboard" class="btn green">Back To Dashboard</button>
@endsection

@section('scripts')
<script>
$("#again").off("click").on("click",function(){
    window.location.href = "/projects/create";
});
</script>
@endsection
