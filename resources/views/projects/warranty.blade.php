@extends('projects.create')

@section('formContent')
<h2>Warranties</h2>
<div class="table-wrapper">
    <table>
        <thead>
            <tr>
                <th>Waranty Type</th>
                <th>Delete</th>
            </tr>
        </thead>
    </table>
</div>
<div id="warrantyTypeContainer" class="form-wrapper">
    <form>
        <div class="form-group">
            <label for="zip" class="col-md-4 control-label">Add Warranty Type </label>

            <div class="col-md-6">
                <select id="warrantyType" class="form-group">
                    <option value="0">Select Warranty Type</option>
                    <option value="dmGutter">Duck Matte Gutter</option>
                    <option value="dmSiding">Duck Matte Siding</option>
                    <option value="pvcGutter">PVC Gutter</option>
                    <option value="pvcSiding">PVC Siding</option>
                    <option value="roofing">Roofing</option>
                </select>
            </div>
        </div>
    </form>
    <button class="btn yellow" id="addWarranty">Add</button>
</div>

<div id="warrantyForm" class="form-wrapper hidden">
    <h2></h2>
    <form></form>
    <button class="btn yellow" id="submitWarranty">Add</button>
</div>


@endsection
@section('scripts')
    <script>
        $.get( "/projects/session", function( data ){

            $("table").DataTable( {
                destroy: true,
                data: data,
                "bProcessing": true,
                destroy: true,
                DT_RowId: 'id',
                columns: [
                    { "data": "warranty" }
                ]
            });
        });
        $("#addWarranty").off("click").on("click",function(){
            $("#warrantyTypeContainer").addClass("hidden");
            value = $("#warrantyType").val();
            if(value !== "0"){
                if(value === "dmGutter"){
                    $.get( "/warranties/create/type/dmGutter", function( data ) {
                        $("#warrantyForm").children("form").html(data);
                        $("#warrantyForm").removeClass("hidden");
                    });
                }
                if(value === "dmSiding"){
                    $.get( "/warranties/create/type/dmSiding", function( data ) {
                        $("#warrantyForm").children("form").html(data);
                        $("#warrantyForm").removeClass("hidden");
                    });
                }
                if(value === "pvcGutter"){
                    $.get( "/warranties/create/type/pvcGutter", function( data ) {
                        $("#warrantyForm").children("form").html(data);
                        $("#warrantyForm").removeClass("hidden");
                    });
                }
                if(value === "pvcSiding"){
                    $.get( "/warranties/create/type/pvcSiding", function( data ) {
                        $("#warrantyForm").children("form").html(data);
                        $("#warrantyForm").removeClass("hidden");
                    });
                }
                if(value === "roofing"){
                    $.get( "/warranties/create/type/roofing", function( data ) {
                        $("#warrantyForm").children("form").html(data);
                        $("#warrantyForm").removeClass("hidden");
                    });
                }
            }
        });
        $("#submitWarranty").off("click").on("click", function(){
            data = $("#warrantyForm").children("form").serialize();
            $.ajax ({
                url: "{{route('sessionStorage')}}",
                data: data+"&_token={{ csrf_token() }}",
                type: "POST",
                success: function(response){
                    console.log(response);
                    $("#nextButton").removeClass("hidden");
                    $.get( "/projects/session", function( data ){
                        types = data['types'];
                        console.log(types.length);
                        $.each(types,function(key,type){
                            type['DT_RowId'] = type['type'];
                            type['delete'] = "<button class='btn red delete'>Delete</button>";
                        });
                        $("table").DataTable( {
                            destroy: true,
                            data: types,
                            "bProcessing": true,
                            destroy: true,
                            DT_RowId: 'id',
                            columns: [
                                { "data": "type" },
                                { "data": "delete" }
                            ]
                        });
                    });
                }
            });
            $("#warrantyForm").children("form").html("");
            $("#warrantyForm").addClass("hidden");
            $("#warrantyTypeContainer").removeClass("hidden");
        });
        $("table").off("click").on("click", ".delete", function(){
            data = $(this).parent().parent().attr("id");
            $.ajax ({
                url: "{{route('sessionStorage')}}",
                data: "warranty="+data+"&_token={{ csrf_token() }}&type=removeWarranty",
                type: "POST",
                success: function(response){
                    console.log(response);
                    $.get( "/projects/session", function( data ){
                        types = data['types'];
                        if(types.length === 0){
                            $("#nextButton").addClass("hidden");
                        } else {
                            $("#nextButton").removeClass("hidden");
                        }
                        $.each(types,function(key,type){
                            type['DT_RowId'] = type['type'];
                            type['delete'] = "<button class='btn red delete'>Delete</button>";
                        });
                        $("table").DataTable( {
                            destroy: true,
                            data: types,
                            "bProcessing": true,
                            destroy: true,
                            DT_RowId: 'id',
                            columns: [
                                { "data": "type" },
                                { "data": "delete" }
                            ]
                        });
                    });
                }
            });
        });
        $("#nextButton").off("click").on("click", function(){
            window.location.href = "/projects/create/review";
        });
    </script>
@endsection
