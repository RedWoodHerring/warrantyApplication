@extends('projects.create')

@section('formContent')
<h2 id="topHeader">Review</h2>
    <div class="reviewGroup">
        <h2>Customer Info</h2>
        <div class="infoItem">
            <h3>Customer Name</h3>
            <p>{{$data['customer']['name']}}</p>
        </div>
    </div>
    <div class="reviewGroup">
        <h2>Primary Contact Info</h2>
        @if($data['config']['customer'] === "new")
                <div class="infoItem">
                    <h3>First Name</h3>
                    <p>{{$data['customer']['contacts']['fName']}}</p>
                </div>
                <div class="infoItem">
                    <h3>Last Name</h3>
                    <p>{{$data['customer']['contacts']['lName']}}</p>
                </div>
                <div class="infoItem">
                    <h3>Email</h3>
                    <p>{{$data['customer']['contacts']['email']}}</p>
                </div>
                <div class="infoItem">
                    <h3>Phone</h3>
                    <p>{{$data['customer']['contacts']['phone']}}</p>
                </div>
        @else
            @foreach($data['customer']['contacts'] as $key => $item)
                @if($item['type'] === "Primary")
                    <div class="infoItem">
                        <h3>First Name</h3>
                        <p>{{$item['fName']}}</p>
                    </div>
                    <div class="infoItem">
                        <h3>Last Name</h3>
                        <p>{{$item['lName']}}</p>
                    </div>
                    <div class="infoItem">
                        <h3>Email</h3>
                        <p>{{$item['email']}}</p>
                    </div>
                    <div class="infoItem">
                        <h3>Phone</h3>
                        <p>{{$item['phone']}}</p>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
    <div class="reviewGroup">
        <h2>Project Address</h2>
        <div class="infoItem">
            <h3>Street Address</h3>
            @if($data['config']['customer'] === "new")
            <p>{{$data['customer']['address']['street']}}</p>
            @else
            <p>{{$data['customer']['address']['address']}}</p>
            @endif
        </div>
        <div class="infoItem">
            <h3>City</h3>
            <p>{{$data['customer']['address']['city']}}</p>
        </div>
        <div class="infoItem">
            <h3>State</h3>
            <p>{{$data['customer']['address']['state']}}</p>
        </div>
        <div class="infoItem">
            <h3>ZipCode</h3>
            <p>{{$data['customer']['address']['zip']}}</p>
        </div>
        <div class="infoItem">
            <h3>County</h3>
            <p>{{$data['customer']['address']['county']}}</p>
        </div>
        @if($data['config']['billingAddress'] === "same")
            <h3>Same As Billing Address</h3>
        @endif
    </div>
    @if($data['config']['billingAddress'] !== "same")
        @if(isset($data['config']['billing']))
            <div class="reviewGroup">
                <h2>Billing Address</h2>
                <div class="infoItem">
                    <h3>Street Address</h3>
                    @if($data['config']['billing'] === "new")
                    <p>{{$data['customer']['billing']['street']}}</p>
                    @else
                    <p>{{$data['customer']['billing']['address']}}</p>
                    @endif
                </div>
                <div class="infoItem">
                    <h3>City</h3>
                    <p>{{$data['customer']['billing']['city']}}</p>
                </div>
                <div class="infoItem">
                    <h3>State</h3>
                    <p>{{$data['customer']['billing']['state']}}</p>
                </div>
                <div class="infoItem">
                    <h3>ZipCode</h3>
                    <p>{{$data['customer']['billing']['zip']}}</p>
                </div>
                <div class="infoItem">
                    <h3>County</h3>
                    <p>{{$data['customer']['billing']['county']}}</p>
                </div>
            </div>
        @endif
        @endif
    <div class="reviewGroup">
        <h2>Warranties</h2>
        @foreach($data['warrantys'] as $key => $row)
            @if($key === "dmGutter")
                <h3 id="topHeader">Duck Matte Gutter</h3>
            @endif
            @if($key === "dmSiding")
                <h3 id="topHeader">Duck Matte Siding</h3>
            @endif
            @if($key === "pvcGutter")
                <h3 id="topHeader">PVC Gutter</h3>
            @endif
            @if($key === "pvcSiding")
                <h3 id="topHeader">PVC Gutter</h3>
            @endif
            @if($key === "roofing")
                <h3 id="topHeader">Roofing</h3>
            @endif
            @foreach($row as $label => $item)
                <div class="infoItem">
                    <h3>{{$label}}</h3>
                    <p>{{$item}}</p>
                </div>
            @endforeach
        @endforeach
    </div>
    <div class="control">
    <input type="checkbox" id="verify">
    <p>I Certify That This Information Is Correct</p>
</div>
@endsection

@section('scripts')
<script>
$('#verify').change(function() {
    if(this.checked) {
        $("#nextButton").removeClass("hidden");
    } else {
        $("#nextButton").addClass("hidden");
    }
});
$("#nextButton").off("click").on("click",function(){
    $.ajax ({
        url: "{{route('projectSubmission')}}",
        data: "_token={{ csrf_token() }}",
        type: "POST",
        success: function(){
            window.location.href = '/projects/create/completed';
        }
    })
});
</script>
<style>
    .reviewGroup{
        display:flex;
        flex-direction:column;
        justify-content: center;
        align-items: center;
        width: 100%;
    }
    .reviewGroup h2 {
        color: #bfa819;
    }
    .infoItem{
        display:flex;
        justify-content: space-between;
        max-width: 50%;
        width: 50%;
        background-color: rgba(0,0,0,.2);
        align-items: center;

    }
    .infoItem h3 {
        margin-right: 4em;
        padding-left: 1em;
        color: #fff;

    }
    .infoItem p {
        padding-right: 1em;
    }
    .control {
        display: flex;
        justify-content: space-around;
        align-items: center;
        width: 50%;
        margin: 0 auto;
    }
    #form-navigation{
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
    }
    #topHeader{
        color: #fff;
        text-align: center;
    }
</style>
@endsection
