@extends('projects.create')

@section('formContent')
<h2>Billing Address</h2>
@if($customer !== "0")
    <div class="table-wrapper">
        <table>
            <thead>
                <tr>
                    <th>Street Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Zip</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="form-wrapper hidden">
        <form></form>
    </div>
    <button class="btn yellow" id="newAddress">Create A New Billing Address</button>
    <button class="btn yellow hidden" id="existingAddress">Use Existing Billing Address</button>
    <input type="hidden" id="addressID"/>
@else
    <div class="table-wrapper hidden">
    </div>
    <div class="form-wrapper">
        <form></form>
    </div>
@endif



@endsection
@section('scripts')
    <script>
        @if($customer !== "0")
        // Pull Listing of Customers
        $.get( "/addresses/{{$customer}}/json", function( data ) {
            $("table").DataTable( {
                destroy: true,
                data: data,
                "bProcessing": true,
                destroy: true,
                DT_RowId: 'id',
                columns: [
                    { "data": "data.address" },
                    { "data": "data.city" },
                    { "data": "data.state" },
                    { "data": "data.zip" }
                ]
            });
        });
        @else
        $.get( "{{route('addresses.create')}}", function( data ) {
            $("form").html(data);
            $(".form-wrapper").removeClass("hidden");
            $(".table-wrapper").addClass("hidden");
            $("#nextButton").removeClass("hidden");
            $("select[name='billing']").parent().parent().addClass("hidden");
        });
        @endif

        //Add Customer ID to input
        $("table").off("click").on("click", 'tbody tr', function(){
            addressID = $(this).attr('id');

            $("table").children("tbody").children("tr").each(function(){
                if($(this).hasClass("selected")){
                    $(this).removeClass("selected");
                } else {
                    $(this).addClass("selected");
                }
            });

            if(addressID !== ""){
                $(this).addClass("selected");
                $("#addressID").val(addressID);
                $("#nextButton").removeClass("hidden");
            }

        });

        //Create New Customer Button click
        $("#newAddress").off("click").on("click",function(){
            $(this).addClass("hidden");
            $("#existingAddress").removeClass("hidden");
            $.get( "{{route('addresses.create')}}", function( data ) {
                $("form").html(data);
                $(".form-wrapper").removeClass("hidden");
                $(".table-wrapper").addClass("hidden");
                $("#nextButton").removeClass("hidden");
                $("select[name='billing']").parent().parent().addClass("hidden");
            });

        });

        //Return to Existing Customers
        $("#existingAddress").off("click").on("click",function(){
            $(this).addClass("hidden");
            $("#newAddress").removeClass("hidden");
            $(".table-wrapper").removeClass("hidden");
            $("form").html("");
            $(".form-wrapper").addClass("hidden");
            $("#nextButton").addClass("hidden");
            $.get( "/addresses/{{$customer}}", function( data ) {
                $("table").DataTable( {
                    destroy: true,
                    data: data,
                    "bProcessing": true,
                    destroy: true,
                    DT_RowId: 'id',
                    columns: [
                        { "data": "data.address" },
                        { "data": "data.city" },
                        { "data": "data.state" },
                        { "data": "data.zip" }
                    ]
                });
            });
        });

        //Next Button click
        $("#nextButton").off("click").on("click", function(){
            if($(".table-wrapper").hasClass("hidden")){
                console.log("hi");
                data = $("form").serialize()+"&type=newBilling";
                url = "/projects/create/warranty/";
            }

            if($(".form-wrapper").hasClass("hidden")){
                data = "id="+$("#addressID").val()+"&type=existingBilling";
                url = "/projects/create/warranty/";
            }
            $.ajax ({
                url: "{{route('sessionStorage')}}",
                data: data+"&_token={{ csrf_token() }}",
                type: "POST",
                success: function(response){
                    window.location.href = url;
                }
            })
        });
    </script>
@endsection
