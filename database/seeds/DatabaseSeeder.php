<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'Joshua',
          'username' => 'josh',
          'password' => bcrypt('Penguin45'),
          'type' => 'A'
      ]);
      DB::table('users')->insert([
          'name' => 'Jordan',
          'username' => 'jordan',
          'password' => bcrypt('Results42!'),
          'type' => 'A'
      ]);
      DB::table('users')->insert([
          'name' => 'FranchiseTest',
          'username' => 'franchiseTest',
          'password' => bcrypt('password'),
          'type' => 'F'
      ]);
      DB::table('franchises')->insert([
          'user_id' => '3',
      ]);
    }
}
