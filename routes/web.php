<?php

Route::get('/login', function () {
    return view('auth.login');
});
Route::get('/style', function () {
    return view('body.style');
});

Auth::routes();

Route::group( ['middleware' => 'auth' ], function(){

    Route::get('/', function(){
      return view ('dashboard');
    })->name('home');

    //
    //  Users
    //

    // Resource Controller
    Route::resource('/users', 'userController');

    //Produce user form based on type
    Route::get('users/typeCreate/{type}', 'userController@typeCreate');

    //-----------------------------

    //
    //  CUSTOMERS
    //

    //view customer Contacts
    Route::get('/customers/createSingle', 'customerController@createSingle')->name('customers.createSingle');

    //view customer Contacts
    Route::get('/customers/listing', 'customerController@listing')->name('customers.listing');

    // Resource Controller
    Route::resource('/customers', 'customerController');

    //view customer Contacts
    Route::get('/customers/listing', 'customerController@listing')->name('customers.listing');

    //view customer Contacts
    Route::get('/customers/{id}/contacts', 'customerController@contacts')->name('customers.contacts');

    //update customer contacts
    Route::put('/customers/{id}/contacts', 'customerController@updateContact')->name('customers.contacts.update');

    //creation form for new customer contact
    Route::get('/customers/{id}/contacts/create', 'customerController@createContact')->name('customers.contacts.create');

    //change customer contact to Primary
    Route::put('/customers/{id}/contacts/master/{contactKey}', 'customerController@primaryContact')->name('customers.contacts.master');

    //Store customer Contact
    Route::post('/customers/{id}/contacts/create', 'customerController@storeContact')->name('customers.contacts.store');

    //remove customer contact
    Route::delete('/customers/{id}/contacts/{contactKey}', 'customerController@deleteContact')->name('customers.contacts.destroy');

    //show customer warranties
    Route::get('/customers/{id}/warranties', 'customerController@warranties');

    //-----------------------------

    //
    //  Addresses
    //

    //view customer Contacts
    Route::get('/addresses/{id}/json', 'addressController@json')->name('addresses.json');

    Route::get('/addresses/create', 'addressController@create')->name('addresses.create');

    Route::get('/addresses/{id}', 'addressController@index')->name('addresses.index');



    //view customer addresses
    Route::get('/customers/{id}/addresses', 'addressController@index')->name('customers.addresses');

    //creation form for new customer address
    Route::get('/customers/{id}/addresses/create', 'addressController@create')->name('customers.addresses.create');

    //update customer address
    Route::put('/customers/{id}/addresses', 'addressController@update')->name('customers.addresses.update');

    //Store customer address
    Route::post('/customers/{id}/addresses/create', 'addressController@store')->name('customers.address.store');

    //change customer address to master
    Route::put('/customers/{id}/addresses/master', 'addressController@masterAddress')->name('customers.address.master');

    //remove customer address
    Route::delete('/customers/{id}/addresses', 'addressController@destroy')->name('customers.addresses.destroy');

    //-----------------------------

    //
    //  Franchises
    //

    // Resource Controller
    Route::resource('/franchises', 'franchiseController');

    //-----------------------------

    //
    //  WARRANTIES
    //


    // Resource Controller
    Route::resource('/warranties', 'warrantyController');

    //Customer Creation with warranty redirect
    Route::get('/customers/create/{redirect}', 'warrantyController@warrantyCreateCustomer')->name('warranty.customer.create');

    //warranty form with new customer and addresses
    Route::get('warranties/create/newCustomer/{id}/{address}', 'warrantyController@warrantyNewCustomer');

    //Show Search View for Customers
    Route::get('warranties/create/customers', 'warrantyController@warrantyCustomer');

    //Show Type warranty type selector
    Route::get('/warranties/create/type', 'warrantyController@warrantyType');

    //Show Warranty Form of a specific type
    Route::get('/warranties/create/type/{type}', 'warrantyController@warrantyForm');

    //Search Customers
    Route::get('warranties/create/{type}', 'customerController@search');

    //Show Customer Addresses
    Route::get('/warranties/create/addresses/{id}', 'warrantyController@warrantyAddress');

    //-----------------------------

    //
    //  Projects
    //

    //Create: Select Customers
    Route::get('/projects/create', function(){
        session()->forget('project');
        return view('projects.customers');
    })->name("projects.create");

    Route::get('/projects/create/completed', function(){
        session()->forget('project');
        return view('projects.finish');
    });

    //Create: Select Address
    Route::get('/projects/create/{customer}/address', function($customer){
        return view('projects.addresses',compact('customer'));
    });

    //Create: Select Billing
    Route::get('/projects/create/{customer}/billing', function($customer){

            return view('projects.billing',compact('customer'));
    });

    //Create Warranty
    Route::get('/projects/create/warranty', function(){
        $types = array();
        session()->forget('project.types');
        session(['project.types' => $types]);
        return view('projects.warranty');
    });

    //Project Review
    Route::get('/projects/create/review', 'projectController@projectReview')->name('projectReview');

    //Session Storage
    Route::post('/projects/create/', 'projectController@sessionStorage')->name('sessionStorage');

    //Session Access
    Route::get('/projects/session/', 'projectController@sessionAccess')->name('sessionAccess');

    //Project Review
    Route::post('/projects/create/review', 'projectController@store')->name('projectSubmission');


    Route::get('/resources',function(){
        return Redirect::to("resources.php");
    });

    Route::get('/areports',function(){
        return Redirect::to("areports.php");
    });

    Route::get('/freports',function(){
        return Redirect::to("freports.php");
    });

    Route::get('/pdfTest', function(){
        $dompdf = new PDF();
        $view = view('pdf.reviewPage')->render();
        $dompdf = PDF::loadHTML("hello")->output();
        file_put_contents('public/doc1.pdf', $dompdf);

        $pdf = new FPDI();

$pageCount = $pdf->setSourceFile('public/doc1.pdf');
$pageId = $pdf->importPage(1);

$pdf->addPage();
$pdf->useImportedPage($pageId, 10, 10, 90);

$pdf->Output();


        $pdf->Output('page12.pdf');

        // return view('pdf.reviewPage');
    });

});
