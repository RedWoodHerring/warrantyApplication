$(document).ready(function(){
	$(".user-info > a").on("click", function(e) {
	  $(".user-info").toggleClass("open");
	  e.stopPropagation();
	});
	$(document).on("click", function(e) {
	  if ($(e.target).is(".user-info > a") === false) {
		$(".user-info").removeClass("open");
	  }
	});
	//nav
	$('.nav-toggle').click(function(){
		$('#app').toggleClass('nav-open');
		$(this).toggleClass('is-active');
	});
	//sidebar
	//mobile subnav open/close
	$('.subnav-toggle').on('click', function() {
		$(this).parent().closest('.has-submenu').toggleClass('open');
	});
	//datatables
	$('#example').DataTable({
		responsive: true,
		ajax: {
	        url: '../ajax/data.json',
	        dataSrc: 'data'
	    },
		columns: [
	        { data: 'id' },
	        { data: 'orderNumber' },
	        { data: 'name' },
	        { data: 'address' },
	        { data: 'city' },
	        { data: 'state' },
			{ data: 'zipcode' },
			{ data: 'date' },
			{ data: 'action', "orderable": false }
	    ]
    });
});
